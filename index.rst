
.. raw:: html

   <a rel="me" href="https://kolektiva.social/@grenobleluttes"></a>
   <a rel="me" href="https://kolektiva.social/@raar"></a>
   <a rel="me" href="https://framapiaf.org/@goldman_bakounine"></a>
   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://piaille.fr/@ldh_grenoble"></a>


.. find -type f -name '*.png' -size +1M -print0 | xargs -0 -n1 mogrify -resize 750

.. figure:: images/crha_logo.png
   :align: center


|FluxWeb| `RSS <https://antiracisme.frama.io/linkertree/rss.xml>`_

.. _glieres_crha_2023:

============================================================
**Rassemblement des Glières des 19, 20 et 21 mai 2023**
============================================================

- http://www.citoyens-resistants.fr
- https://www.youtube.com/@CRHA_RassemblementdesGlieres/videos
- https://nitter.net/CRHA_glieres/rss
- https://fr-fr.facebook.com/rassemblementdesglieres/

::

    129 rue des Fleuries , Thorens-Glières, France
    citoyen.2008@yahoo.fr


.. figure:: 05/21/images/resistantes_fin_de_prises_de_parole.png
   :align: center


.. figure:: images/carte_thorens_glieres.png
   :align: center

   Thorens-Glières, cinéma Le Parnal, monument en mémoire des résistants espagnols, MJC et salle Tom Morel https://www.openstreetmap.org/#map=18/45.99726/6.24842


.. toctree::
   :maxdepth: 5

   05/05
   photos/photos
   videos/videos
   media/media
