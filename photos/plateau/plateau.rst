
.. _photos_plateau:

=========================================
**Photos sur le plateau**
=========================================

- https://nitter.net/CRHA_glieres/#m

.. raw:: html

   <iframe width="800" height="600" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" sandbox="allow-forms allow-scripts allow-same-origin" src="https://www.geoportail.gouv.fr/embed/visu.html?c=6.329477350590899,45.96409759654725&z=17&l0=GEOGRAPHICALGRIDSYSTEMS.MAPS::GEOPORTAIL:OGC:WMTS(1)&l1=GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2::GEOPORTAIL:OGC:WMTS(1)&l2=ORTHOIMAGERY.ORTHOPHOTOS::GEOPORTAIL:OGC:WMTS(1)&permalink=yes" allowfullscreen>
   </iframe>


   https://app.webcam-hd.com/conseil-general-74/cg74_plateau-des-glieres, https://app.webcam-hd.com/conseil-general-74/depart-des-pistes



Militantes et militants rassemblées
=====================================

.. figure:: militantes_rassemblees.png
   :align: center


🎤 Ce dimanche 21 mai, les chants et les paroles de #resistances ont résonné au Plateau des Glières.
De la solidarité, de l'émotion...

Merci à tou.te.s
D'être là, d'avoir pris la parole, écouté et/ou chanté.

Nous en sortons uni.e.s, dynamisé.e.e, fort.e.s...


.. figure:: fin_de_prises_de_parole.png
   :align: center
