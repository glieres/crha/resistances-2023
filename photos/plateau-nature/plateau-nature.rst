.. index::
   ! Conteneur 2e guerre mondiale

.. _photos_plateau_nature:

=========================================
**Plateau nature**
=========================================


Vallon
======


.. figure:: vallon.png
   :align: center


Restes d'un conteneur de la 2e guerre mondiale
==============================================

.. figure:: restes_conteneur_1.png
   :align: center


.. figure:: restes_conteneur_2.png
   :align: center


.. figure:: restes_conteneur_3.png
   :align: center

.. figure:: environnement_conteneur.png
   :align: center

