
.. _photos_thorens:

=========================================
**Photos à Thorens-Glières**
=========================================


.. figure:: ../../images/carte_thorens_glieres.png
   :align: center

   Thorens-Glières, cinéma Le Parnal, monument en mémoire des résistants espagnols, MJC et salle Tom Morel https://www.openstreetmap.org/#map=18/45.99726/6.24842



Forum des résistances d'aujourd'hui
=======================================


.. figure:: forum_le_media.png
   :align: center


.. figure:: forum_des_resistances.png
   :align: center



Des militantes et militants au coeur du rassemblement
========================================================

.. figure:: militantes_et_militants_au_coeur_du_rassemblement.png
   :align: center


.. figure:: militant_grenoblois.png
   :align: center


Vue extérieure des chapiteaux et vue sur les montagnes des Bornes
===================================================================

.. figure:: vue_chapiteaux_et_montagnes.png
   :align: center



.. figure:: vue_chapiteaux_et_montagnes_2.png
   :align: center


🎶 Stéphanie Questana ("Le sirop de la rue") instaure une ambiance qui met les cœurs en résistance
=====================================================================================================

- https://nitter.net/LeMediaTV/status/1660269638251433985#m

🎶 Stéphanie Questana ("Le sirop de la rue") instaure une ambiance qui
met les cœurs en résistance sur la place principale du @CRHA_glieres. ⤵️

.. figure:: stephanie_questana.png
   :align: center

   https://nitter.net/LeMediaTV/status/1660269638251433985#m


Gilles Perret
================


.. figure:: gilles_perret.png
   :align: center


Mémoire de la Résistance espagnole
===================================


.. figure:: resistance_espagnole.png
   :align: center


Mémoire CNR
===================================

.. figure:: cnr.png
   :align: center
