.. index::
   pair: Media ; Le-media

.. _le-media:

=========================================
**Le Media**
=========================================

- Instagram : https://www.instagram.com/lemedia_tv/
- Facebook : http://bit.ly/FacebookLeMedia
- Twitter : https://twitter.com/LeMediaTV
- Youtube : http://bit.ly/YouTubeLeMedia
- Peertube : https://video.lemediatv.fr
- Telegram : https://t.me/LeMediaOfficiel
- TikTok : https://www.tiktok.com/@lemediatv

- https://nitter.net/Cemil/rss
- https://nitter.net/Nicomay/rss


▶ Soutenez Le Média :

👉 https://soutenez.lemediatv.fr (CB - SEPA - Chèque)
👉 https://dons.lemediatv.fr (CB - SEPA - Chèque)
👉 https://fr.tipeee.com/le-media (CB - Paypal)


.. figure:: images/stand_le_media_cemil.png
   :align: center


Objectifs : informer sur le projet inédit que nous portons et faire signer notre pétition
==============================================================================================

- https://nitter.net/LeMediaTV/status/1660271343500140545#m

Côté stands, celui du MEDIA fait le plein avec @Nicomay et @Cemil.
Objectifs : informer sur le projet inédit que nous portons et faire
signer notre pétition.

- http://unetelepourvous.lemediatv.fr/

.. figure:: images/stand_le_media_1.png
   :align: center

   https://nitter.net/LeMediaTV/status/1660271343500140545#m
