
.. _crha_2023_05_19:

=========================================================================================================================================
|crha| Vendredi 19 mai 2023 **Rassemblement des Glières à Thorens-Glières Film et théâtre**
=========================================================================================================================================

- https://citoyens-resistants.fr/spip.php?article659
- https://citoyens-resistants.fr/IMG/pdf/tract_recto_verso_impression_crha_2023.pdf
- https://citoyens-resistants.fr/IMG/pdf/crha_programme_mai_2023.pdf
- https://www.openstreetmap.org/#map=18/45.99726/6.24842

.. figure:: images/affiche_glieres_2023_05_19__21.png
   :align: center


.. figure:: images/glieres_2023_05_19.png
   :align: center

   Programme du vendredi 19 mai 2023, https://citoyens-resistants.fr/IMG/pdf/crha_programme_mai_2023.pdf


.. figure:: images/carte_thorens_glieres.png
   :align: center

   Thorens-Glières, cinéma Le Parnal, monument en mémoire des résistants espagnols, MJC et salle Tom Morel https://www.openstreetmap.org/#map=18/45.99726/6.24842



Vendredi 19 mai 2023 17h00 Cinéma Le Parnal **Ernest et Célestine** À partir de 5 ans
========================================================================================

Au cinéma Le Parnal **Ernest et Célestine** à partir de 5 ans. 2022
1h19- suivi d'une discussion avec les enfants.

Vendredi 19 mai 2023 20h00 Théâtre **Le monde commence aujourd’hui**, compagnie La Cahute dans la salle Tom Morel
====================================================================================================================

- https://www.youtube.com/watch?v=y45ymgrXmr8
- https://en.wikipedia.org/wiki/Jacques_Lusseyran

.. figure:: images/la_cahute.png
   :align: center

   Le monde commence aujourd’hui, https://www.youtube.com/watch?v=y45ymgrXmr8

Lucas, 20 ans, découvre par hasard les textes de **Jacques Lusseyran, un
écrivain résistant aveugle qui a connu l’horreur de la déportation**.

Cette découverte l’amène à remettre en question la manière dont il est
en train de vivre sa vie.

Il se rend compte que son existence est en train de lui échapper, qu’il
file vers une vie toute tracée qu’il n’a jamais véritablement choisie.

Avec Lucas, nous suivons les questionnements d’une jeunesse contemporaine
qui cherche du sens et ne se sent pas heureuse dans la vie qu’on lui
propose.

En parallèle, nous découvrons la vie de `Jacques Lusseyran <https://en.wikipedia.org/wiki/Jacques_Lusseyran>`_, deux histoires
qui se croisent et se répondent pour exprimer de manière joyeuse et
puissante l’ardent désir de vivre de la jeunesse et l’urgence de se révolter
quand celui-ci est menacé




