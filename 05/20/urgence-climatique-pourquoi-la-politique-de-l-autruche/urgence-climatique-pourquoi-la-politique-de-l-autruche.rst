
.. _urgence_climatique_2023_05_20:

=======================================================================================================
Samedi 20 mai 2023 **17h00, Chapiteau : Urgence climatique : pourquoi la politique de l’autruche ?**
=======================================================================================================


Participantes/participants
============================

avec:

- Catherine Larrère
- Raphaël Larrère
- Stéphane Labranche
- (Dominique Bourg absent)


Vidéo
=======

- https://piped.video/TLpheE22RZY
- https://www.youtube.com/watch?v=TLpheE22RZY



Transcription Youtube (à travailler :)
==========================================


donc nous accueillons ici euh Catherine larrer qui a philosophe bonjour
spécialiste en éthique de l'environnement et à l'origine de l'essor
de la philosophie sur la protection de l'environnement la prévention des
risques et la justice environnementale vous avez à ça de gauche je suis
malade désolé vous avez à sa gauche son mari Rafael larrer à Grenoble
à l'origine qui donc [Musique] qui a écrit des histoires de la protection
de la nature éthique environnementale qui a été conseiller scientifique
dans les parcs naturels ils ont écrit ensemble le pire n'est pas certain
et le tout dernier qui sortit aujourd'hui du coup j'ai 20 avril le 20 avril
pas aujourd'hui l'éco féminisme voilà donc ça c'était la conférence
précédente il y a pas que Sandrine Rousseau [Rires] [Applaudissements] et
enfin nous avons Stéphane Labranche qui se dit climatologue de la société
c'est bien ça il est sociologue il est nominé au GIEC pour ses travaux et
donc il s'occupe des mécanismes d'acceptation sociale qui conditionnent
la réussite des politiques des politiques publiques écologiques voilà
donc la thématique de cette conférence c'était le la crise climatique si
crise d'ailleurs est le bon mot puisqu'une crise normalement et ce réseau
et on revient à la normale on n'est pas sûr que ce soit le cas la crise
climatique donc c'était politique de l'autruche point de suspension point
d'interrogation enfin ça va être à débattre entre nous voilà enfin entre
nous non moi je vais me contenter de d'arbitrer éventuellement les passages
de micro donc voilà je vous laisse je vous laisse Catherine commencer bien
comme vous voyez on est tous d'accord on va essayer oui Chris climatique
politique de l'autruche bon disons moi j'ai rien contre contre l'emploi du
terme de crise parce que il y a des quantités de crise qui ne se résolvent
pas et bon une crise ça peut durer donc je pense ça il y a pas il y a pas
de raison de par employer le mot bon je pense que si vous êtes là c'est
que vous êtes intéressé par la par la question et que donc vous savez en
gros quel est quel est quel est l'état du monde sur sur le dernier rapport
du GIEC qui a été bon fait il y a trois groupes dans le GIEC donc ça a
été fait au fur et à mesure on a eu le dernier il y a quelques mois bah
il y a de quoi rester au fond de son lit un peu pleurer parce que parce que
les les les conditions sont mauvaises alors ce que qu'on peut dire le titre
de cette de cette de ce débat de cette séance c'est la puissance climatique
qu'est-ce qu'on veut dire quand on dit ça on veut dire si on prend les les
chiffres de de d'émissions de gaz à effet de serre bon je pense que vous
savez tout sur les tous en gros sur la sur la sur la question climatique sur
le dérèglement climatique l'augmentation des des gaz à effet de serre
leur est dans l'atmosphère à un des conséquences diverses alors sur le
le régime climatique pas seulement la hausse des températures mais aussi
l'augmentation des événements extrêmes que ce soit en sécheresse ou en ou
au contraire en innovation que d'autre part c'est pas uniquement un problème
climatique que c'est articulé enchevêtré avec d'autres phénomènes en
particulier d'autres crises en particulier la question de la biodiversité
il y a eu récemment un rapport dirigé par un écologue Vincent de Victor
qui a montré la disparition inquiétante et imposante des oiseaux mais il y
en avait eu aussi sur les mammifères etc bref la situation peut-on dire et
mauvaise et on a quantité d'indicateurs pour le montrer et si on regarde au
même niveau global que celui où on enregistre les problèmes la situation
ne change pas vraiment c'est à dire en particulier sur les sur les sur
le changement climatique les les émissions de de gaz à effet de serre en
fait continue à augmenter et quand elles ont très légèrement diminué
dans dans 2020 2021 au moment du covid elles ont repris très vite et donc
un certain nombre alors que se réunissent régulièrement tous les ans des
conférences internationales bon ce qu'on appelle les cops les Conférences
des Parties à la à la Convention sur le climat ils prennent des engagements
ces engagements ne sont ne sont pas suivis ou pas suffisamment suivi et c'est
en ce sens qu'on peut se dire il y a un changement climatique important ça
bon on dit qu'il y a encore des climatosceptiques on peut en discuter je
crois que tout le monde sait que c'est en train de changer et pourtant on ne
fait rien si on présente la situation comme ça il y a peut-être de quoi
être un peu un petit peu désespéré donc il faut peut-être essayer de
de voir exactement comment les choses se passent moi j'interviendrai si vous
voulez à trois niveaux d'une à premièrement nous avons un une évaluation
globale qui ne nous dit rien sur ce qui se passe à des niveaux plus locaux
on a une sorte d'accumulation des des chiffres et cette accumulation des
chiffres est inquiétante mais je dirais de la même façon que on critique
le le l'estimation du PNB du produit national brut parce qu'elles ne tient
pas en compte la diversité des choses on peut dire aussi qu'on ne peut pas
s'en tenir à la seule estimation globale et qu'un certain nombre de choses
qui sont faites en particulier au niveau de la biodiversité ne sont pas
prises en considération là là la deuxième donc on peut essayer peut-être
d'avoir d'autres éléments d'information sur ce qui se passe et pas en rester
uniquement à l'impression effectivement extrêmement inquiétante que donne
les chiffres globaux deuxième remarque il va pour nous tous à moi moi nous
compris ici que bien la situation c'est une évaluation globale par des
scientifiques que ce soit le GIEC le Groupe intergouvernemental d'experts de
l'évolution climatique que ce soit pour la biodiversité les buzz donc qui
font un état du monde et à partir de cet état du monde prise de conscience
changement climatique décision et actions des gouvernements politiques dans
une telle approche et on n'a pas tort on pourra dire les scientifiques font
leur travail et les gouvernements pas vraiment mais c'est là pour la question
est pourquoi là il y a plusieurs types d'explications si Dominique bourre
avait été là bon ils ont donné une explication qui est de dire nos nos
systèmes nos démocraties et représentatives ne savent pas prendre en compte
le long terme pour apprendre à le prendre en compte moi je donnerai une autre
explication et je reviendrai qui est le dire dans des pays le gros problème
c'est un problème politique c'est un problème d'inégalité sociale dans
des pays [Musique] envie de faire grand chose et donc on va dire bah si c'est
là qui se passe rien or ça me paraît passé à côté de quelque chose qui
est très important mais alors qui se remarque pas au niveau global mais qui
a des des résultats locaux qui est qui a quantité d'initiative citoyenne un
peu partout sous des formes très diverses ça ça peut être la permaculture
de nouvelles façons de produire qui sont aussi de nouvelles façons de faire
société ça peut être des actions beaucoup plus violentes et militante qui
peuvent se terminer par un par un succès Notre-Dame-des-Landes finalement
le gouvernement a renoncé à agrandir le l'aéroport mais il n'y a pas
seulement en France il y a partout dans le monde parce que un économiste
écologique comme Johan Martinez allier appelle des vous demande de justice
environnementale des mobilisations partout dans le monde sur des questions
qui peuvent être des questions de distribution d'eau qui peuvent être des
questions plus directement économiques ou plus nettement environnementale
et ça fait dans le monde un réseau d'actions extrêmement important donc je
crois qu'il faut prendre ces trois niveaux c'est à dire il y a effectivement
le diagnostic global il y a effectivement l'action des gouvernements mais
il y a aussi qui est faible on pourra y revenir mais il y a aussi toutes les
mobilisations citoyennes et bon qui ne se traduisent pas en en chiffre globaux
à la façon dont la on peut totaliser globaliser les émissions de gaz à
effet de serre mais qui sont à mon avis un élément extrêmement important
pour avoir un peu confiance dans l'avenir je vais enchaîner pour ajouter
un quatrième niveau en fait je travaille essentiellement en sociologie
je travaille aussi avec des économistes incroyables et aussi des psychos
cognitivistes des gens qui regardent comment le cerveau fonctionne juste pour
être sûr que tout le monde me comprenne quand je parle je vais parler à
partir de je conduis à 38 ou 39 projets de recherche en sociologie ce qui
veut dire que j'ai parlé à plus de 450 personnes en face à face depuis
une vingtaine d'années en leur demandant comment vous avez des comment vous
êtes déplacé aujourd'hui mais pourquoi vous aimez autant la voiture pendant
que vous êtes dans les transports en commun et le vélo ah vous avez peur
des accidents oui c'est pas contre les climat ce que j'essaie de comprendre
c'est quelle est la place de l'environnement du climat dans la décision
quotidienne des individus mais aussi des entreprises des collectivités
territoriales comment le fonctionnement de l'entreprise de collectivité
peut amener ou non des changements même si on désire changer juste une
anecdote fonctionnement du cerveau mais je participais à l'élaboration
d'un Plan Climat et en fait il y avait un projet d'agrandissement d'une
voirie et on sait bien que quand vous ajoutez une ligne ne file en plus
oui pendant quelques mois c'est plus difficile il y a moins de bouchons
et un an deux ans après on a encore plus de voitures et exactement sur
le même bouchon mais payer en plus donc plus de voitures ça on sait ça
c'est partout pareil dans le monde c'est une constante c'est mondiale et ben
les gens de voirie ils savaient pas apparemment donc il voulait agrandir le
machin et en fait deux portes plus loin dans le corridor il y avait des gens
qui s'occupaient de la mobilité et les deux équipes sont plutôt copains
copines ils mangent ensemble ils boivent le café ensemble mais il se parle
pas au boulot et ils sont pas sur les mêmes listes d'adresses internet
mail pour pouvoir échanger ce ressort est-ce qu'on est-ce qu'on sera pas
une réunion et donc j'essaie de comprendre parfois ces petites anecdotes
ces petits détails qui font que c'est dans le dans le diable est dans les
détails mais je le vois beaucoup mais pour revenir à la au 4e niveau il
faut comprendre un peu comment cerveau fonctionne et là j'ai des débats
aussi avec les copains neurobes cognitivistes et je vous dirai là où je
suis pas d'accord avec eux mais il y a une chose qui semble à peu près
universelle je suis à peu près tout le monde le savoureux paresseux le
cerveau n'aime pas dépenser beaucoup d'énergie sur beaucoup de choses
à la fois pourquoi parce qu'il est très occupé en fait à gérer toute
votre physiologie et à gérer toutes les la communication les perceptions
les décisions à prendre etc etc et donc le cerveau a tendance face à un
problème à se dire regarde à côté le champ d'invisibilité ça existe
ça s'appelle un problème qu'on ne veut pas assumer et donc je reviens à
cette idée de l'autruche faire l'autruche c'est quoi en fait c'est juste
une vieille légende complètement fausse qui dit que les autruches vont se
la tête dans le sable et c'est absolument pas vrai quand il y a un prédateur
qui passe si c'était vrai il n'y aurait plus d'autruches sur la planète
on est bien d'accord je vous laisse aussi penser au parallèle qu'on peut
faire avec notre propre situation avec la biodiversité de climat et notre
capacité ne pas regarder les choses en face mais la vraie question pour moi
c'est pourquoi on ne regarde pas le problème en face et si on le regarde
en face est-ce que là c'est la prise de conscience est-ce que si c'est
nécessairement à partir de ce que je vais faire en prendre les bonnes
décisions ben non et donc en fait cette qualité du cerveau à être de plus
efficace et donc paresseux possible est un des fondements si vous voulez de ce
problème de l'autruche si je regarde pas le problème il n'existe pas donc
j'en ai pas un moment donné la science nous dit merci en fait ça existe
mais le cerveau d'autres choses à faire que d'être scientifiques parce
qu'en fait notre neuro-cognition ce n'est qu'une petite partie des facteurs
qui rentrent en compte quand on ajoute au quotidien qu'on se réveille si on
décide de manger de la viande ou pas au quotidien si on a envie de se faire
un barbecue si on se promène en vélo etc les émotions jouent en rôle
beaucoup plus important et donc quel est le rôle des émotions là-dedans
alors pour revenir au climatocepticisme il y a une grosse étude mondiale
qui est sorti dernièrement qui montre une augmentation en France de 10%
des climatosceptiques depuis deux ans la plus grosse partie ce n'est pas
des gens qui nie le changement climatique ce sont les gens qui ni la place de
l'humain dans le changement climatique pourquoi l'autruche si je suis en partie
responsable en tant qu'humain du problème ou là il faut que je fasse quoi
et deuxième et ça c'est une hypothèse qu'on a comme bruit était capable
de vérifier on est certain à penser que si on présente et ça en psycho
on le montrait on présente à quelqu'un un problème a priori insoluble la
préférence de dire ah ben tiens je vais aller faire autre chose et en fait
le présenter le changement climatique comme un problème catastrophique
sans solution ni alternative réalisable plus ou moins à laquelle je ne
peux pas vraiment contribuer c'est le meilleur c'est le meilleur moyen pour
créer de l'angoisse si vous créez de l'angoisse vous créez du déni et
donc on joue à l'autruche ok donc ça c'est au niveau du cerveau je vais
juste finir sur un truc et ensuite passer la parole je veux juste monter un
peu de niveau je vous raconte un peu l'histoire du vélo tout à l'heure en
fait je ne travaille pratiquement que ce changement climatique est dans ma
signature mail j'ai une citation de Frank Herbert de dunes qui dit il faut
apprendre à penser climat donc ça depuis 40 ans que je lis le bouquin six
fois je pense pour moi c'est clair mais malgré le fait que je pense qu'il
m'a je bouffe l'image je réfléchis j'écris climat je voilà vous savez
pourquoi je fais du vélo parce que je vais me faire du vélo ça n'a rien
à voir avec le climat et la raison pour laquelle j'ai arrêté de faire du
crier du vélo mon dernier n'a rien à voir avec le climat non plus et donc
quelle est la place du climat dans nos décisions quotidiennes en partie
c'est non entre nos deux oreilles mais quand nos deux oreilles sortent dans
la rue et se mettent sur un vélo et qu'on a peur de se faire frapper par une
voiture le climat n'a plus d'important dernière anecdote je sais très bien
qu'avoir un enfant c'est exploser mon bilan carbone j'ai fait ou peut-être
l'autruche en partie et je suis devenue papa Yumi moi malgré tout ce que je
sais sur la biodiversité le changement climatique les risques internationaux
de conflits de machins il y a une seule chose je me suis imaginé avec un
enfant dans les bras les émotions pris le dessus merde le climat je sais
pas si c'était de l'autruche mais il y avait une forme de Digne en tout
cas sur tout ce que je sais par ailleurs et pourtant les 5e et 6e rapport
du GIEC je me suis payé donc je vais juste en terminer là c'est pour
vraiment à compléter ce que vous disiez tout à l'heure justement avec ce
peut-être un quatrième niveau ce qui se passe entre les deux oreilles et
qu'est-ce qui se passe quand nous on rencontre la réalité extérieure les
infrastructures les autres personnes etc oui ben je vais je vais enchaîner
puis je vais enchaîner sur ce sur ce que vous venez de dire on pourrait
passer au tutoiement ça me paraît alors il y a des points sur le café
d'accord et pas d'accord mais ça me paraît que ce qu'on nous a mis tous
dans la tête que la question climatique c'est la question d'un diagnostic
scientifique d'autres il faut prendre conscience et ensuite on va prendre les
mesures qu'il faut et la difficulté qu'il y a quand on en arrive au niveau
individuel aux mesures qu'il faut prendre or effectivement bon il y a le la
enfin il y a un livre qui s'appelle le syndrome de l'autruche d'un certain
Georges Marchal et qui raconte exactement ça qu'est-ce qui montre il montre
qu'il y a une distance énorme entre le tableau global qu'on nous fait du
changement climatique je crois que bon on a en gros on a confiance dans les
scientifiques et la réalité qu'on vit et pour moi le problème il est là
c'est à dire qu'on tousse et dans cette salle on se sent comme comment on
va faire pour appliquer ce qu'on nous dit alors que l'important c'est plus
de voir là où il se passe des choses c'est pas des gens qui appliquent ce
que je prends un exemple aussi c'est pas des gens qui appliquent ce qu'on
leur a dit de faire d'en haut c'est des gens qui font ce qui ont envie de
faire et je prends un exemple de de d'une enquête qui a été faite sur
des sur l'agriculture biologique alors c'est une enquête qui a été faite
sur la la la la la la France qui est limitrophes de la de la Belgique de
Luxembourg et de la Suisse et on demandait donc il y a des agriculteurs
qui sont lancés dans la dans l'agriculture biologique ce qui demande du
courage de regarder les choses en face etc et quand on leur demande et
pourquoi vous avez fait ça la première réponse c'est pas du tout pour
diminuer les pesticides ou c'est pas pour sauver la planète ils disent
on a fait ça parce que on voulait retrouver notre autonomie on voulait
c'était l'agriculture dans la dans laquelle ils étaient pris auparavant
étaient étaient une agriculture intégratrice industrialisée c'est à
dire on leur donnait les semences il les plantait chercher les résultats
et ils en avaient marre de ça et il voulait disait-il dans leurs enquêtes
réapprendre à se tromper réapprendre à être responsable de quelque chose
et alors c'était pas être indépendant parce qu'ils avaient des quantités
de bien avec leur voisin que faire de l'agriculture biologique justement ça
demande de d'avoir beaucoup plus de réseau de proximité mais il faisait ça
parce qu'ils aimaient ça il faisait pas ça sur bon ce que nous savons nous
aussi les uns les autres pesticides et autres phytosanitaires bah c'est à
la fois mauvais pour le climat et mauvais pour la biodiversité il faisait
quelque chose d'abord parce que c'était bon pour eux et je pense que c'est
ça qui est important finalement et à ce moment-là c'est là que je serai
peut-être pas tout à fait d'accord avec toi à ce moment-là ce qui se
passe c'est pas uniquement un agrégat de décision individuelle il y a
des actes collectifs on agit collectivement et à ce moment-là il y a pas
besoin forcément de d'arriver à une grande masse pour qu'il y ait des choses
qui changent je pense mais je pense que là dessus on est il y a l'idée il
faut faire ce que disent les scientifiques mais ça marche pas je voudrais
préciser que au fond l'impuissance dans laquelle se trouve se trouve pour
changer les choses tiens en partie au fait que on réfléchit on continue
nous-même nous sommes un peu prisonniers de ce qui s'est passé on continue
de réfléchir au niveau global et alors effectivement le GIEC c'est très
bien le GIEC c'est très bien il y a une robuste de vitesse scientifique
suffisant de ces scénarios et ces modèles globaux ne discerne pas bon
très bien discerner peuvent être des éléments de lancement d'alerte pour
dire attention si ça continue la température moyenne va augmenter et si
elle dépasse de 2 degrés il va y avoir une multiplication d'événements
climatiques catastrophiques de d'incendie de sécheresse épouvantable ou
d'inondation comme il y en a en ce moment en Italie donc voilà comme il y en
avait comme il y a eu à la Roya avec la tempête Alex etc et donc à partir
de là on sait ça mais à partir de là le GIEC est incapable de dire il ne
peut pas le dire d'ailleurs d'une part qu'elle va être l'évolution précise
de la météorologie pas possible que il y a eu une année particulièrement
fraîche qui interviennent à un moment donné qu'on s'en rend pas de même
que avait pas prévu qui ferait 38 degrés en Sibérie en 2020 alors ces
scénarios il manque la nécessité de limiter les émissions de gaz à effet
de serre mais ça permet pas d'indiquer la stratégie que les pays devraient
ou pourraient adopter pour y contribuer parce que les situations de ces
pays sont trop diverses ils n'ont ni le même niveau de consommation ni les
mêmes structures économiques ni les mêmes capacités d'investissement donc
le GIEC ne dit rien de ce qu'il faudrait faire alors c'est normal puisque
penser globalement c'est oublier la diversité réelle du monde d'un monde
qui est hétérogène nous sommes quand même par exemple nous dans des pays
développés et nous bénéficions nous sommes des privilégiés par rapport
à l'essentiel de l'Afrique dans la mission du GIEC comme de l'IPBES libpes
c'est le GIEC de la biodiversité c'est pas de prescrit ce qu'il faut faire
c'est de fournir aux décideurs politiques les éléments pour prendre une
décision et comment prendre des décisions au niveau global au niveau global
on peut pas prendre de décision alors il y a effectivement des cops on en
a entendu plusieurs fois la Coop etc bon certaines ont plus ou moins bien
réussi et l'épée les pays s'entendent à prendre des engagements ouais
problème c'est que les engagements qu'ils ont accepté ils se gardent bien
de les appliquer c'est quand même ennuyeux ça tient d'ailleurs à ce que
aucune sanction n'est prévue si bien que toute nation concerne la liberté
de persévérer dans le business Asus word de n'avancer que par petits pas
ou de pratiquer le greenwashing pour faire croire qu'elle agit et sans rien
faire sans agir vraiment donc s'il y a une efficacité par exemple des cops
c'est que quand il y a un accord signé il y a des ONG des associations de
protection de l'environnement qui dresse des procès à leur gouvernement
respectifs qui les gagnent et qui peuvent glisser juridiquement les pouvoirs
publics à songer même très bonnement dans le sens des engagements qui
sont pris eux-mêmes alors il y a une impuissance en hiver internationale
et certains mais je pense que Dominique Bourg des fois est pas loin de été
pas loin de le penser pensait que un gouvernement mondial de l'environnement
permettrait d'améliorer les choses mais à ce niveau global il serait tout
aussi impossible de déterminer les objectifs précis des différentes
régions du monde et en plus plus on monte en globalité plus on perd en
démocratie un gouvernement mondial ne pourrait être que despotique donc
moi je m'en tiendrai là pour l'instant pour échapper à la sidération
que peut provoquer c'est un puissant il faut qu'on retrouve confiance dans
notre capacité notre celle des citoyens d'agir et donc revenir au local
en gardant en tête les enjeux globaux ce qui revient selon l'expression de
Bruno Latour a localisé le global plus tard j'enchaîne ouais l'image si je
peux me permettre d'intervenir bien sûr on a quand même eu le problème
de la convention citoyenne en France qui était quand même alors c'était
local relatif mais en tout cas au niveau du au niveau du pays cette convention
citoyenne on a annoncé enfin très très curieusement alors que c'était des
gens enfin choisi de façon relativement aléatoire ils sont tous arrivés à
des conclusions qu'on aurait pu connaître avant mais au moins ils ont été
construites par des gens issus de milieux différents avec des convictions
différentes au départ en étant éduqués sur les questions climatiques et on
a eu donc 150 ou 449 je ne sais plus préconisations et les préconisations
je sais pas elles sont fournit un tiroir enfin il s'est pas passé grand
chose derrière donc il y a quand même un problème d'inaction et sans qu'on
attend d'un gouvernement mondial ou quoi que ce soit même au niveau local
il y a ce problème d'indaction alors qu'ici on est quand même je pense
qu'il y a pas mal de personnes qui sont pas loin de l'activisme ou qu'ils
sont carrément et qu'ils ont conscience que d'après certaines études il
semblerait qu'il y a trois et demi pour cent de la population qui est prête
à essayer de changer les choses on peut arriver à basculement donc ils ont
envie d'entendre aussi que on va pouvoir faire avancer le schmilblick vraiment
très optimiste il faudrait un peu plus que ça quand même on vit dans un
système Super intégré mondial qui nous amène à bouffer à consommer à
acheter des portables à mettre des nouvelles applis à faire de la 5G etc
etc etc et donc la confrontation on est censé rentrer dans une société de
se briter là l'injonction contradictoire et les majeures quand même mais
je vais quand même revenir c'est pas tout à fait vrai que le GIEC ne nous
dit pas quoi faire le GIEC nous dit quoi faire c'est dit changer de mode
de vie ça dit pas comment ça c'est la différence donc là il a ce que
je voulais dire c'est que ça c'est l'accord entre les scientifiques et les
politiques les scientifiques doivent pas prescrire donc ils disent on prescrit
pas mais ils montrent quelque chose à faire parce que quand on dit changer
de mode de vie ça veut dire changer de civilisation s'il y a pas quelque
chose de performatif là-dedans je sais pas ce que c'est mais ça dit pas
quoi faire et donc on a testé ça en fait avec des chefs de de stratégie
d'adaptation partout en France et on leur a demandé de quoi vous avez besoin
pour développer une stratégie d'adaptation et en fait ils nous ont dit
des outils des bonnes pratiques des exemples des trucs en fonctionnaires et
un retour sur expérience des échecs ils ont pas demandé beaucoup plus de
Demi scientifiques sauf pour certaines régions où il y avait pas assez de
données scientifiques mais une fois qu'on a la donnée scientifique minimale
suffisante ça suffit et en fait je renvoie ça la plupart d'entre nous on
utilise un ordinateur qui est capable de faire de la programmation aussi on
conduit des voitures qui est capable de réparer le moteur donc voilà moi
donc vous voyez ce que je veux dire c'est qu'on a pas besoin d'avoir toute la
connaissance scientifique pour me donner agir et donc il faut avoir un minimum
quand même parce que sinon on prend les mauvaises décisions et plutôt que
de faire de la diversification énergétique et d'énergie renouvelables
on se dit diversification énergétique on rajoute du charbon ben non on
rajoute pas du charbon et ça c'est un effet positif des négociations il
y en a eu des effets négatifs positifs des négociations quand même au
niveau international elles vont passer loin moi j'ai même plutôt tendance
à dire heureusement qu'on ne pas tendu les négociations internationales
et accords de perron 2015 pour faire un truc au niveau territorial parce
que les territoires ont commencé bien avant bien avant à expérimenter à
tenter des choses donc autant dans les collectivités territoriales que des
initiatives citoyennes mais n'empêche que ces acteurs pour n'importe qui que
tendait quelque chose de niveau d'innovant je parle pas de technologiquement
je parle socialement parlant qu'est-ce que ça veut dire socialement parlant
de dire bon à partir de maintenant pour un producteur agricole allez hop je
me mets production bio ouh là c'est un peu plus compliqué qu'on le pensait
il y avait beaucoup qui rentre en dépression parce qu'il est passé de pognon
qui rentre parce qu'ils sont pas capables de faire vivre leur famille donc
il lâche le taux de suicide hyper élevé il y a quand même des aspects
psychologiques et sociologiques qui sont importants prendre en compte qu'on
parle de changement et des difficultés à changer et c'est là pour moi ou
peut-être plus qu'ailleurs l'État et les collectivités territoriales les
pouvoirs publics n'appuie pas assez les innovations au niveau territorial
c'est pas que les collectivités territoriales ne font pas d'innovations
moi je travaille depuis 2003 là dessus je peux vous dire que j'ai vu
beaucoup de changements passer pas assez vite mais pas mal de changements
mais il y a pas suffisamment d'appui sur ce initiatives là qui une fois
diffusé et mais à grande échelle pourra avoir un vrai effet mais là où
je te rejoins complètement c'est que ces initiatives-là sont porteuses
d'espoir psychologique parce que face parce que face à la catastrophisme
j'adore Dominique mais face au catastrophisme de Dominique Bourg on a envie
de suicider quand on sort de la salle on est encore là il y a pas que ça
il y a aussi des bonnes initiatives il y a des choses qui se passent qui sont
positives d'ailleurs depuis 2015 je refuse de donner une image catastrophique
des situations même si je la connais en conférence publique je donne que
les trucs qui fonctionnent plus ou moins correctement le pire n'est pas
certain pour pour disque mettre en cause cette idée que si on en reste à
une certaine vision on se dit que c'est foutu qui a plus rien à faire et que
donc si vous voulez quand même gênant de prendre comme image d'une conduite
quelque chose qui est faux les autruches se mettent pas la tête dans le
sol donc laissons les autruches à leur rationalité ce que ce que ce que je
veux dire c'est que la la l'image qu'on nous impose je vais revenir sur la
convention citoyenne c'est y aller scientifiques il y a les politiques ça
c'est des experts les politiques c'est ceux qui savent ils ont le savoir de
la situation à partir de là ils développent des politiques qui aurait pu à
partir de là il développe des politiques qui d'autre qu'ils appliquent il y
a tout un tas de d'échelle et puis en bas les gens n'ont plus rien non plus
qu'à faire ce qu'on leur dit de faire et qu'ils ne font pas parce qu'ils sont
avec un citron c'est ça l'image qu'on nous met dans la tête on veut pas etc
qu'est-ce qui qu'est-ce qui se passe il se passe exactement le contraire qui
est-ce qui se mobilise en disant aux politiques appliquer vos vos engagements
donc ceux qui résistent à appliquer les engagements c'est les gouvernements
et ceux qui oblige à essaie d'obliger à appliquer les les engagements pris
par les gouvernements c'est les manifestations de jeunes sur le climat c'est
l'ensemble des procédures judiciaires qui ont été menées pour obliger
les gouvernements à appliquer leur à appliquer leurs engagements et ça a
été en France la faire du siècle le gouvernement a été condamné ça a
été le le la commune de Grande-Synthe qui a attaqué le gouvernement et le
gouvernement a été condamné donc s'il y a des gens qui veulent agir et qui
trouve que le gouvernement va trop lentement c'est l'ensemble des citoyens
c'est-à-dire c'est exactement le contraire de la présentation qu'on nous
donne et si vous voulez la conférence des citoyens sur le climat bon ça
montre que si on réunit des gens au hasard on n'a pas choisi des écolos
convaincus sur donc sur un échantillon représentatif s'ils ont les moyens de
s'informer finalement ils prennent de bonnes décisions alors bon tout est pas
excellent on peut discuter bon qu'est-ce qu'on constate il y avait récemment
à un article qui reprenait toutes les 152 142 je sais plus bon et que quand
le gouvernement dit ben en fait on en a appliqué 80% oui bah c'est du genre
on en a pris une et puis on l'a on l'a on l'a réalisé à 100% ou bien bon
la la l'idée de baisser la vitesse sur autoroute bon ça pas été repris si
bien que la conférence sur le climat ça apparaît comme un moyen un peu de
légitimer une action gouvernementale qui est complètement technocratique
et qui en fait ne fait rien donc je crois qu'on est tous les trois d'accord
là-dessus s'il y a de l'espoir c'est du côté des mobilisations citoyennes
sous des formes extrêmement différentes et c'est ça effectivement qu'il
faut soutenir mais il faut pas attendre que l'État se soutienne il faut
forcer l'État à soutenir je reviens sur le sur la je m'arrête et si je
reviens sur l'agriculture biologique normalement ils sont soutenus Phénix
financièrement mais c'est pas payé il y a des engagements ils sont soutenus
mais ils sont beaucoup moins soutenus que l'agriculture productiviste nous
savons tous que on n'a pas de ministre de l'Agriculture on a la FNSEA à la
place mais on nous disait ce que je voulais dire petite parenthèse sur le le
débat citoyen sur le climat alors il y a un premier une première chose qui
est le côté très démagogique de Macron qui a dit en transfère à ça sans
filtre c'est impossible c'était anticonstitutionnel même puisqu'il devait
y avoir nécessairement le filtre du Parlement enfin bon il le savait très
bien donc démagogie et de ceci dit le discours enfin moi je me souviens que
je discutais avec des gens qui ont été très proches de l'organisation de
ce de ce truc est-ce qu'il disait il était convenu dès le départ qu'on
ne parlerait du nuclé on parlerait pas du nucléaire et que deuxièmement
c'est une affaire d'état et on ne parlerait pas non plus de des impôts et
de la fiscalité c'est bizarre quoi parce que quand même la fiscalité est
un moyen éventuellement de modifier l'attitude des des entreprises enfin
voilà donc juste une parenthèse pour ceux qui n'étaient pas là ce matin
sur la conférence sur l'eau Julie trétier nous disait par exemple que sur
le plan o dans le Gers ou quelque part dans le Sud-Ouest quelque chose comme
ça paragraphe douce de la page 13 comme elle disait il y avait quelque
chose qui concernait donc toutes les restrictions toutes les restrictions
possibles sauf pour l'agriculture sous contrat donc les gros trucs etc et
par contre les petits agriculteurs les agriculteurs passaient en bio etc
ils avaient droit aux restrictions mais pas les gros pas ceux qui étaient
le plus responsable du manque d'eau oui alors oui je voulais préciser quand
même un peu sur les initiatives citoyennes alors certains bons tu as parlé
de l'agriculture biologique s'inscrivent dans des projets réformés par
l'État mais il y a plein d'autres qui se déploient dans les marges de
liberté que laisse encore les pouvoirs publics je dis encore mais voir
bien d'autres résulte de lutte sociale et politique pour défendre des
activités un mode de vie qui est menacé par l'extension d'activités
économiques tout particulièrement ça se remarque dans les incursions
extractivistes sur des terres qui étaient laissés avant à leurs habits
habituels traditionnel les affrontements d'ailleurs peuvent devenir très
violents et c'est par exemple le cas en Amérique latine ou en Afrique où les
grandes entreprises minières et pétrolières empiète de plus en plus sur
les territoires indigènes multiples dans leur forme et dans leur objectif
alors on peut songer par exemple il y a des mouvements paysans sur tous les
continents qui se développent pour promouvoir des formes d'agroécologie
de penser aussi à l'extension de la permaculture qui se présente tout en
étant tout en se disant apolitique comme un art de réhabiter la nature en
prenant soin de la terre des autres humains des autres cumin on peut voir
le mouvement des villes de voilà des Villes en Transition et se référer
comme tu l'as fait tout à l'heure à tous les mouvements que que Johann
martylaz allié a mis dans son Atlas non c'est des actions qui redessine
les humains les rapports des humains entre eux et leur rapport avec leur
milieu de vie et qui esquisse une véritable altérité sociale qui fait que
on peut le retrouver alors en France il y a eu le nasale et c'est pas pour
c'est pas un hasard si notre ministre de l'Intérieur dit il faut plus de
ZAD puisque ça a marché la transition écologique est mal engagée dans
les politiques nationales et internationales mais elle s'effectue dans un
foisonnement d'expérience collective ils sont à la fois des façons de
vivre autrement et des expériences autonomes et démocratiques c'est donc
dans cette diversité de situations que se développe les revendications et
les expériences autonomes qui sont des formes de lutte contre le changement
climatique la question est alors la suivante est-ce qu'on parviendra à
mettre en réseau la multitude de mouvements d'expérience citoyenne de
lutte qui localise le global alors on pourrait globaliser local en pensant
et en agissant localement on agirait aussi globalement mais pour ça il faut
mettre en réseau tous ces mouvements et c'est peut-être parce que c'est un
des c'était un des espoirs de soulèvement de la terre que l'on a pensé le
abandonné enfin tout du moins le rendre dissoudre le soulèvement de la terre
enfin que le gouvernement a pensé et pense toujours dissoudre le second de
la terre si je pense que la pluie qui a apporté Valérie Malson Delmotte au
soulèvement de la terre et quelque chose d'extrêmement important dans la la
façon dont les les différents activismes se mettent en rapport les uns avec
les autres si je peux me permettre de mouiller la chemise et de dire quelque
chose qui sort peut-être pas populaire dans une assemblée comme celle-ci
je suis parfaitement d'accord avec la mise en réseau et des initiatives
citoyennes il y en a c'est très bien mais il faut pas penser que c'est la
tendance majeure de la dynamique de cette société pas parce que les gens
sont contre le changement mais parce qu'ils ont autre chose à faire des
fois c'est vraiment anecdotique mais on est tenté les couches lavables et
quand on a vu que dans la boîte il y avait trois couleurs de couches puis
qui fait la commencer à organiser les couleurs de gauche selon je sais pas
trop quoi avec la charge mentale qu'on avait on est abandonné et là on
nous demande de changer de mode de vie ok ce que je veux dire par là c'est
qu'il ne faut pas penser surtout dans une audience comme celle-ci que les
restants de la planète nous ressemblent dans le sens que le restant de la
planète a peut-être d'autres préoccupations ou de l'indifférence ou de la
résistance ou autre chose à faire ou peut-être si juste la difficulté à
faire à arriver à ses fins de mois il y a tout ça donc il y a une grande
diversité en face il faut être quand même compétition et empathique
face à des gens qui peut-être peur de perdre le niveau de vie qu'ils ont
en ce moment qu'ils ont peur que dans 10 ans ils pourront plus le prendre
d'avion donc on a une partie des jeunes qui prennent encore plus l'avion
maintenant parce qu'ils se disent que dans des Alpes on peut voyager donc
ils augmentent leur bilan effet de serre c'est pas parce qu'il croit pas il
croit au changement climatique mais quoi surtout qu'il y aura des politiques
publiques hyper coercitives qui vont arriver et que le prix va augmenter et
qui pourront plus le faire et donc je veux bien il faut célébrer c'est
mobilisations citoyennes et ses initiatives il faut les diffuser il faut
en parler il faut aussi en tant que les analyser je suis dans un projet de
recherche en ce moment sur ces petites initiatives qui émergent c'est-à-dire
ok pourquoi ça fonctionnait avec vous quel facteur quel obstacle vous avez
rencontré comment vous les avez résolus parce que seule fois qu'on va avoir
identifier les obstacles tout ce que peuvent fonctionner comment les ils ont
résolu mais pour la prochaine vague ça va être deux fois plus facile donc
il faut quand même avoir cette démarche mais ne pas penser que le restant
de la planète allait même préoccupations que nous parce que ça veut dire
en fait qu'on envoie un message d'injonction qui va être super mal reçu et
qui ne peut en fait qu'engendrer des résistances donc moi j'ai eu pas mal
d'étudiants qui sont devenus végétariens pendant leur parcours à Sciences
Po Grenoble et je peux vous dire que quand il me parle de leur dynamique
pour devenir végétarien juste face à leur famille et des explications
des justifications qui doivent donner c'est hyper saoulant ils veulent juste
manger des carottes et des patates on n'est pas obligé de leur donner de la
viande ils sont obligés de se justifier et ils sont accusés de procéditisme
simplement parce qu'ils disent ah ben non pas de viande et donc ça c'est
ça c'est des interactions quotidiennes qui rentrent de changement ou peuvent
rendre le changement difficile au-delà de l'argent qui n'existe pas ou qui
peut exister bon c'est pas qu'ils existent ou qu'on sait pas comment elle
est l'atteindre etc etc il y a tous ces éléments là qu'il faut prendre en
compte donc pour moi c'est important de bien comprendre il y a une grande
diversité il faut être capable de parler à cette diversité et avoir un
seul message unique souvent la planète ne peut pas fonctionner avec tout
le monde il faut sortir de cette logique ça ne peut pas fonctionner il y a
d'autres manières d'arriver toute façon la planète on la sauvera pas on
n'a pas besoin très bien merci mais ce que je voulais dire ce que je voulais
dire c'est que tu as parfaitement raison de dire il y a une très grande
diversité donc il y a pas un modèle unique et c'est pas une solution pour
tout le monde d'être végétariens alors on peut on peut montrer que diminuer
sa consommation de viande ça des effets très importants sur les émissions
de gaz à effet de serre bon c'est une des choses importantes mais ça veut
pas dire à partir de là qu'il y a un modèle unique et alors où je dirais
peut-être un peu différemment de toi c'est que ailleurs dans le monde il
y a des gens qui souffrent beaucoup plus durement du changement climatique
que nous parce qu'ils sont dans dans des régions tropicales par exemple ou
là ou la mer a monté plus parce qu'ils ont moins de moyens de s'y adapter
et et et et donc moins de résilience et cela lutte encore plus et s'engage
dans des changements bon c'est pour ça que je parlais de Johan Martinez cette
économiste qui ça fait ça fait un moment qui travaille sur ces questions et
il a écrit un livre qui s'appelle l'écologie des pauvres et où il a montré
que bon il a écrit ça à l'époque on disait l'écologie c'est un lustre de
riche il a dit non mais il y a une écologie des pauvres c'est à dire quand
quand les dérèglement environnemental dérèglement climatique rend la vie
impossible mais on lutte et donc il y a aussi toutes ces luttes là et qui
sont plus radicales et plus importantes que les nôtres et quand bon quand
j'évoquais l'idée de mettre en réseau ce que l'on pouvait mettre en réseau
l'ensemble de ces initiatives citoyennes de ces luttes etc bien évidemment
ça voulait pas dire que c'était des luttes qui avaient pour objectif de
sauver le climat certaines de ces luttes et la plupart d'entre elles même
en particulier de on a parlé de la lutte contre l'extractivité c'est de
défendre son milieu de vie et en fait considère que on a parfaitement
le droit de défendre son milieu de vie et se défendre ce milieu de vie
c'est modifier ses relations à la nature ces relations aux autres hommes
et ses relations au nom humain voilà et tout ça ça participe d'un grand
mouvement et c'est un grand mouvement qui n'est effectivement pas qui n'a pas
pour objectif de sauver la planète mais qui a pour objectif de rendre la
Terre habitable j'ai cru comprendre que vous avez des solutions si je peux
commencer par quelques unes mais je me situe vraiment dans une sociologie
hyper pratique au pratique et périphérique quotidienne et pour moi parce
que c'est le pour trouver des solutions qui vont être utilisées il faut
comprendre le problème réel qui est derrière donc vous avez difficulté
à utiliser le vélo en ville parce que vous avez peur de vous prendre une
voiture dans la gueule mais vous faites des pièces sécurisées il faut
commencer vraiment par là pour moi les grandes solutions génériques ne
peuvent pas marcher sur plus du territoire encore plus sur les questions
d'adaptation d'impact changement climatique qui varie d'un territoire à un
autre ou même sur un même territoire un algoritivo d'un peu plus bas qui se
retrouve avec Belledonne d'un côté Chartreuse de l'autre une vallée dans le
milieu 3 impacts du changement climatique différents et en fait ils sont en
voie d'oiseaux où ils sont à quoi 400 km à peine et donc il faut vraiment
être territorialisé là-dessus ça je et c'est pour ça que pour moi c'est
important d'y aller non seulement au niveau d'un territoire médiali aussi
par secteur d'activité le meilleur moyen pour convaincre quelqu'un de manger
moins de viande c'est de lui faire une bonne bouffe végétarienne et ensuite
on lui donne la recette c'est pas de faire grand discours ça sert à rien
ensuite un univers un échelle un peu plus grand qu'on regarde des nouveaux
objectifs de la neutralité carbone en France on est censé arriver donc à
une neutralité carbone je vous expliquerai rapidement on y mettant on va dire
une bêtise allez 100 millions de tonnes par année il faut pas émettre plus
de 100 millions de tonnes donc on n'arrive pas à zéro émission on arrive à
un bilan 0 neutre il y a deux deux gros enjeux qu'il va falloir aborder là
maintenant et on est en retard c'est la question bien entendu des modes de
production agricole et de la consommation qui va avec de bouffe et l'autre
c'est la rénovation énergétique des bâtiments et sur la question des
rénovation énergétiques des bâtiments j'ai assisté à des négociations
de copropriété là-dessus avec une agence locale de l'énergie là l'agence
locale d'énergie sont un argument unique pratiquement c'est bon pour le
climat et ensuite c'est bon pour votre portefeuille et en face on avait des
gens qui disent oui mais combien ça va me coûter je vous jure on avait il
y avait cinq personnes autour de la table sur les cinq il y avait trois qui
avaient des doctorats il y en a pas un des trois qui étaient capable de dire
à combien d'aide financière ils avaient droit c'était tellement complexe
comme information à aller chercher et analyser que ils étaient pas capable
d'avoir même cette idée là donc autrement dit incapable de savoir combien
d'années ça va prendre avant d'avoir un retour sur investissement correct
et alors si déjà avec des doctorats sont pas capables d'aller chercher une
information qui devrait être simple à aller chercher vous pouvez imaginer
quand on a peur d'aller chercher de l'information qu'on sait pas comment
l'analyser comment c'est angoissant et donc déjà simplification d'un mode
d'aller pour aller chercher guichet unique des choses comme ça il faut que
ça existe c'est toujours pas là et oui et ben voilà c'est ça et donc
en fait il y a des il y a des solutions comme ça qui existent mais il faut
qu'elle soit sectorielles et il faut qu'elle soit opérationnelle beaucoup
d'entreprises le demandent aussi il faut leur donner un peu de temps pour
changer certes elle les demande toujours au moins 5 ans surtout pour les
grandes entreprises mais les petites ont besoin elles ont pas les ressources
humaines le temps de de faire leur boulot de payer les 10 employés à la fin
du mois et ensuite de rentrer dans une processus de transition énergétique
dans leur process c'est juste trop compliqué le dossier est trop compliqué
à monter les enjeux sont trop compliqués à comprendre il faut une aide
là-dessus j'ai rencontré pas mal d'entreprises qui veulent faire des
choses et puis j'aborde ils abandonnent par par des pieds en fait c'est
décourage en fait ça c'est du courage en général Dominique quand même
on peut passer aux questions si vous on peut passer aux questions si vous
le souhaitez je vous remercie parce que j'ai essayé d'intervenir ce matin
j'ai essayé d'intervenir ce matin à la conférence sur l'eau et puis bon
j'ai pas pu alors moi je suis là depuis ce matin j'entendais conférence
dans ces très intéressant parfois porteur d'espoir parfois un peu plus
plombant il y a un mot qui n'est pas là c'est le mot politique le gros
mot je vais parler à l'échelon locale je fais aussi partie de collectif
d'associations qui s'épuise littéralement qui s'épuise à lutter contre des
projets qui ont été décidés je vais citer des noms parce que maintenant
il faut citer des noms un conseil départemental dirigé par Monsieur Sadier
et son copain coquin monsieur Wauquiez à la région et je et on s'épuise
à lutter alors plateau de Sonic Beauregard l'autoroute Chablais enfin bon
je vais pas tous les citer il y en a trop et du coup je me dis mais mince
pourquoi est-ce que constamment ce sont je précise je me suis présenté
à des élections une liste où il y avait plein de mouvements différents
et nous avons fait 20% en ayant tous les comment dire tous les soutiens
qu'on pouvait imaginer politique etc sauf évidemment ce productiviste de
droite bon et bien voilà et je souhaiterais que que cette question soit
posée parce que personnellement c'est ces personnel je pense que maintenant
face à la gravité de la situation même si on peut se réjouir il y a des
initiatives ça fait du bien ça fait plaisir mais mettre en réseau toutes
ces initiatives tant que ça ne fait pas système on continue d'aller dans
le mur quoi et on y va de plus en plus vite donc je me dis il faut sortir le
gros mot et dire maintenant politiquement parce que qu'on le veuille ou non
et bien c'est pas les gens dans les associations et c'est pas les citoyens
qui votent les lois ou qui décident de de ces fameux alinéa 12 machins
de l'article c'est bien les les décideurs et les élus donc est-ce que ma
question c'est ça est-ce qu'à un moment donné dans des assemblées comme
ici il faudrait pas sortir le gros mot qui est politiquement qu'est-ce qu'on
fait [Applaudissements] bon je sais pas il y a deux réponses à vous faire
la réponse on a parlé que de ça c'est-à-dire d'un bout à l'autre on
vous a jamais dit que c'était une question de technique on vous a jamais
dit que c'était une affaire de nature bon donc d'un bout à l'autre c'est
une question politique c'est une question politique dès le départ parce
que ce qui nous a la question qu'on propose toujours pourquoi on en est là
c'est une série de décisions politiques c'est pas le le développement
naturel de je sais pas quoi c'est une série de décisions politiques et ce
qui peut nous en sortir c'est une série d'actions politiques et ce que vous
citez c'est des actions politiques alors à différents niveaux bon alors ça
c'est dit c'est pas un gros mot je pense que d'entrée de jeu on a abordé
la question comme une question politique à partir de là qu'est-ce qu'il
faut faire je vous dirai c'est pas à moi de vous le dire je crois que vous
vous avez montré votre engagement on a envie de vous dire mais continue
parce que je vais pas vous dire il y a des minorités qui pardon il y a des
minorités qui gagnent parce que par exemple les luthraciales aux États-Unis
ça a pas été embarqué par une majorité c'est une minorité qui a réussi
à changer les lois alors on voit bien que la majorité des appâts franchement
intégrés mais en tout cas une minorité a réussi à faire bouger des
lois donc parfois des minorités agissantes arrivent à faire plus qu'une
majorité qu'on essaie désespérément d'éduquer et de convaincre juste
apporter deux éléments il y a quand même une lueur d'espoir la politique
mange par de la politique et non pas du politique sur la politique regardez ce
qui s'est passé au dernier élections municipales avec des écolos qui même
dans des villes on y pensait jamais sont devenus majoritaires et certaines
de ces nouvelles équipes qui n'étaient pas habituées au pouvoir politique
ni à la décision locale ont certainement fait des erreurs parfois en allant
trouver sur certains trucs en se disant allez on y va on nous a élu puis là
on était peut-être pas les lumières de Noël après tout c'est peut-être
pas une bonne idée mais là je vais vous proposer moi je vais vous proposer
un geste hyper évolutionnaire surtout pour vous excusez-moi vous avez entendu
l'accent surtout pour les Français à chaque fois qu'une politique qui est
menée et pour laquelle vous n'êtes pas content vous prenez la rue pas de
problème pourquoi quand il y a une politique pour laquelle vous avez une
approbation pourquoi vous écrivez pas une lettre pour dire bravo pourquoi
ne pas montrer publiquement un support votre soutien pour des politiques qui
vont dans le bon sens pour montrer au restant de population que en fait il y
a des gens contents avec ce qui se passe et que vous n'êtes pas une petite
minorité de rien du tout qui est jamais content parce qu'en fait vous êtes
trop écolo non il y a quand même c'est peut-être la communication non
violente à la Canadienne aussi puis notre message positif machin mais il
y a quand même à la québécoise encore plus je dirais il y a quand même
un truc à jouer là pour moi c'est moi ça fait 23 ans que je suis ici et
je suis encore fasciné par le fait qu'on entend que râler et jamais quand
ça se passe bien quand il y a quelqu'un qui fait une bonne action quand il
y a un bon plan de végétalisation en ville quand les courbes de récré
sont en train d'être végétalisés féliciter vos élus il en falloir
convaincu voilà en Haute-Savoie c'est pas franchement [Applaudissements]
on n'est pas un truc de temps en temps qui sort quoi j'ai envisagé deux
solutions pour essayer de réduire mon impact sur le changement climatique
enfin un autre impact c'était devenir végétarien et manger un riche à
votre [Musique] comme c'est pas trop compatible à votre avis laquelle des
deux méthodes serait la plus efficace ça dépend comment vous cuisiner je
pense qu'Air Beckham vous répondrez manger un riche mais juste un on est
vite quand même rassasié c'est un peu le problème non et même non mais
le je dirais moi le problème c'est de d'enfermer ça alors j'ai dit c'est
politique c'est politique d'un bout à l'autre et et le la mobilisation
climatique ne met pas fin au conflit de classe et aux inégalités sociales
je dis à quel point là là bien au contraire la question des inégalités
sociales étaient déterminante dans la dans la dans l'avancée de d'une
action climatique à quel point bon si on revient sur les sur les gilets
jaunes fin du mois fin du monde mais je pense Bruno Latour avait dit on
n'a jamais vu à quel point la question la question climatique la question
écologique est une question sociale c'est-à-dire que c'est ce que je disais
il y a une écologie des pauvres il y a des gens qui bon on ne peuvent pas
avoir peur de la fin du monde parce que le problème immédiat c'est la
fin du mois mais que la fin du mois un rapport avec la fin du monde et donc
en particulier les les gilets jaunes c'est ressorti après c'est pas à la
taxe carbone qu'ils en avaient c'est le fait que la taxe carbone pesait sur
les plus pauvres et que les plus riches continuent à prendre l'avion etc
donc le on peut pas y avoir de politique écologiques efficace s'il n'y a
pas une politique sociale ça c'est certain alors d'autre part et vice et
Versailles on luttera pas contre les problèmes d'emploi si on prend pas
en compte les climatique environnemental et puis bon je crois que sur les
riches et les pauvres c'est à la fois très très symbolique on l'a vu avec
les histoires des des avions et il y a des il y a des riches qui font comme
s'il y avait pas de changement climatique et ça c'est proprement scandaleux
alors que d'autres en souffrent énormément mais on peut pas en rester à
l'opposition simple des des riches et des peaux d'abord parce que les riches
c'est pas uniquement leur consommation c'est essentiellement leur patrimoine
qui posent problème et à ce moment-là c'est pas simplement leur dire bon
ben prenez plus l'avion vous avez plus le droit d'avoir d'avion personnel
c'est vraiment agir sur des structures et et et et puis aussi et sur leurs
investissements le patrimoine c'est la question des investissements et puis
aussi parce que accuser les riches on a raison mais c'est oublier que nous
aussi on est on est un petit peu profiteur de de la situation que [Musique]
on va pas faire passer tout le monde à aux confessionnal mais on fait pas
tous qu'on pourrait faire et donc il y a aussi cette capacité à prendre
ne pas dire bon c'est la faute des riches et apprendre en charge aussi ce
qu'on fait nous-mêmes si je peux me permettre de revenir sur ce point de la
présence de l'État et de la politique si je fais des choses personnelles je
peux vraiment parler à mon nom personnel si je fais attention à ce que je
plante dans mon jardin en faisant attention au changement climatique qui va
venir en faisant attention au fait que je vais avoir des fleurs sur la plus
longue période possible pour les abeilles toutes ces choses là je les fais
parce que je considère que c'est ma marge de liberté individuelle et que
c'est ma manière de contribuer à une solution et pas aux problèmes sans
qu'on me dise quoi faire par un État ou une politique publique quelconque
et pour moi c'est une marge de plaisir aussi mais une marge de manœuvre de
liberté c'est une manière de pratiquer ma liberté donc c'est comme ça je
le vois donc ça c'est juste pour revenir à ce point deuxième point sur
les gilets jaunes bon ça a été une sacrée claque à l'état ça nous a
donné une claque à nous aussi chercheur on avait tendance jusqu'où gilets
jaunes à un peu éclipsé cette question qu'on était sur les questions
environnementales et les gens qui bossaient sur les questions des inégalités
n'allez pas sur la question environnementale on se parle pas en fait ou très
très peu et là merde un peu plus faire autrement ce qui veut dire que nous
on est obligé de changer des protocoles de recherche on est obligé de poser
les questions différentes dans nos enquêtes mais ça pose problème parce
que qui répond aux enquête c'est pas les classes les plus précaires qui se
méfient des questionnaires et des sociologues qui vont les voir chez eux et
donc nous on a de la difficulté en fait à aller chercher ces informations
dont on a besoin pour éventuellement faire une préconisation de politique
publique qui va prendre en compte les inégalités compensées accompagner
machin et truc mais on a de la difficulté ce qui veut dire que nous on
a besoin de sortir de notre carcan de je sais comment faire ma méthode
de socio habituelle ça a toujours marché oui mais on passe à côté de
quelque chose ce qui veut dire qu'il faut imaginer des nouvelles méthodes
donc là aussi bah certaines d'entre nous on aime ça et certains d'entre
nous on aime moins et ça veut dire aussi toujours avoir ça en tête à
chaque fois qu'on fait une enquête composé question qu'on parle à des
gens c'est super important et ça veut dire avoir des réseaux différents
on peut pas simplement envoyer faire une enquête à grande échelle par
téléphone sur une ville et espérer avoir suffisamment de réponses de
la part des classes précaires il faut aller voir un CCAS aller voir un
bailleur social et le faire en direct bon ben il faut le faire sauf que
ça ça a plus de pognon ça coûte plus de financement donc est-ce que la
ville qui veulent faire ou le bailleur social ou la dame en le pognant pour
le faire non vous voyez en termes de chair mais ça franchement depuis les
gilets jaunes c'est une petite leçon d'humilité et en tout cas moi c'est
clair que je fais maintenant c'est ça fait partie intégrante des questions
autant que je le peux que je pose pour essayer de comprendre les situations
et les contextes différents et les capacités d'action quand les gens mais
aussi leur perception de leur capacités d'action j'ai rencontré un groupe
qui d'ailleurs c'était Annecy qui avait fait depuis mes trois ans un travail
monumental et me racontait tout ce qu'ils avaient fait et puis moi j'étais
je sais pas ce que vous voulez que je vous dise en fait je vois pas ce que
vous pourriez faire plus oui mais on change rien vous réalisez qu'il y a des
équipes qui voudraient faire un dixième de ce que vous avez fait appel RSA
une victoire et donc des fois faut aussi remettre les choses dans le contexte
et cette question justement de la fatigue des associations elle existe aussi
dans certains services de collectivité territoriales j'ai parlé à des gens
qui étaient en burn out vous avez beaucoup quand même parlé de solutions
individuelle et moi je vais sortir un autre gros mot parce que pardon donc un
autre gros mot politique mais politique aussi nationale et internationale parce
que moi je suis un peu étonné quand même c'est bien de nommer l'ennemi et
qu'à mon avis on est quand même dans un système économique de prédation
qui s'appelle le capitalisme donc je sais pas c'est un mot quand même qui
serait bien à dire quoi et donc je suis effectivement je trouve que c'est
intéressant les solutions individuelles de mettre tout ça en réseau etc
c'est très bien mais c'est évident que ça suffira pas je veux dire la
politique de Total elle restera celle qu'elle est la politique des bassines
resterait qu'elle est la case par exemple pour revenir à quelque chose de
très concret de des lignes de SNCF ça va continuer à être ce que c'est
et du coup la parce qu'il y a un peu de la visualisation individuelle du
coup c'est un peu allez il faut qu'on fasse des efforts alors je dis pas
que c'est pas utile mais bon s'il y a plus de lignes SNCF entre deux lieux
le lieu d'habitation et le lieu de travail les gens veulent être obligé
de prendre leur voiture et on n'a pas à les condamner quoi donc moi c'est
non seulement la politique locale mais c'est aussi la politique nationale
je crois que cette bataille moi je trouve que la situation est grave alors
moi je veux bien qu'il faut pas être trop déprimé mais je trouve qu'on
est dans un quand même quand on lit quand on lit les les les dernières
décisions du Jack et je suis d'accord avec le monsieur parce que justement
cette année enfin il me semble que la première année où ils étaient
il faut changer en gros de de système de enfin c'était pas dit comme ça
mais c'est ce qu'elle voulait dire quoi de façon de vivre ils ont été
du coup cassé par les politiques là-dessus donc moi je trouve qu'on a la
situation est hyper grave et qu'effectivement il faut d'autant plus faire des
luttes effectivement sociale écologique au niveau politique et il est très
décevant qu'aux dernières élections ça a été très peu développé
[Applaudissements] alors je crois qu'on vous a pas simplement répondu
au niveau individuel quand on quand on parle bon Notre-Dame-des-Landes
c'était un mouvement collectif avec avec qui a eu des effets politiques
bon le quand vous et avec des résonances internationales quand on parle de
de permaculture bon on n'a pas parlé de bure et de des mouvements contre le
projet CGO c'est pas uniquement individuel on n'est pas en train de vous dire
les petits gestes on est en train de vous dire l'importance des initiatives
citoyennes qui sont pas des initiatives individuelles alors effectivement
la consommation végétarienne ça c'est individuel et encore bon il y a
des représentations sociales et collectives on n'est pas des des individus
à côté les uns des autres capitalisme moi personnellement ça me gêne
absolument pas d'en parler parce que c'est effectivement quand je dis c'est
une série de décisions c'est cet ensemble qu'on appelle le capitalisme
de rapport de production d'exploitation etc bien évidemment une fois ceci
dit une fois qu'on a dit capitalisme on n'a pas dit grand chose parce que
pendant tout le 19e la fin du 19e et le 20e siècle la réponse du Québec
au capitalisme des socialisme bon en matière de productivisme c'est pas
vraiment une réponse donc il y a vraiment à inventer d'autres choses et
donc c'est pas en ce bon on peut donner les mots et mais c'est simplement
ce qu'on est je crois tous tous les trois en train de vous dire c'est on a
beaucoup à apprendre de ce qui est en train de se faire et on a un peu trop
tendance de façon générale à penser que le savoir il est en haut et que
on est là pour apprendre aux gens quoi faire or les gens ils bougent ils
se passent des choses c'est pour ça que je vous parlais de Johan Martinez
allier et qui fait un atlas bon ce qui n'a pas la justice environnementale
c'est conflit environnementaux vous pouvez aller voir sur internet vous
avez il y en a partout dans le monde donc il se passe des choses est-ce
que alors si la question c'est la question de la du rôle des parties dans
un pays comme la France là aussi on peut en discuter je suis pas sûr que
ça soit la solution la plus importante vous pensez que des minorités une
coalition de minorité pourrait finir par renverser la table le problème
vu que nos politiques on voit bien que l'écologie on va se contenter à
l'écologie elle a une elle a gagné le combat culturel puisque maintenant
tout le monde est plus écolo que le voisin ça c'est sûr par contre dans
les urnes franchement c'est pas une évidence et donc c'est peut-être un
problème que si vous voulez les les résultats de de d'Europe Écologie
Les Verts soit pris comme un indice de la conscience écologique du pays
parce que c'est pas vrai un changement vous pensez que les minorités
agissantes je pense alors comme toujours en écologie ou en environnement
il n'y a pas une solution unique il y a on a besoin de tout on a besoin de
l'État sans état on sentir pas on a besoin de pito mais on a besoin de
forcer l'État c'est pour ça que je parlais du du contentieux juridique de
Grande-Synthe ou de la faire du siècle donc on a besoin de faire pression
donc on va pas dire ah bah on a plus besoin de l'État on en a besoin on a
mais effectivement moi je pense que des des minorités peuvent entraîner
un changement de comportement parce que je crois si tu veux si tu avais fait
tes enquêtes je sais pas moi la veille du 14 juillet 89 les gens t'auraient
dit bah non moi je bouge pas et puis le lendemain ça bouge donc il y a il
y a une capacité oui d'exemples et de contagion si je peux me permettre
là dessus sur les niveaux collectifs en une minute vous oubliez peut-être
que au niveau européen aux États-Unis au Canada au Japon en Chine il y a
une objectif de neutralité carbone en 2050 et quand on regarde ce que ça
veut dire ça veut dire qu'en France nous aurons si ça marche et cela c'est
collectif nous aurons 10 minutes de 50% notre consommation viande en 2050 que
nous avons transformé le système thermique mobile notre voiture un système
non thermique non émetteur de gaz effet de serre et ça veut dire aussi que
la rénovation énergétique des bâtiments ont avancé de environ les on
espère 80% en 2050 ça veut dire aussi que on aura des réseaux d'entraide
de réparation de tous les systèmes de consommation mon truc qui marche
plus je m'en achète un autre c'est terminé en 2050 avec la nutrialité
carbone et donc en fait ces initiatives là maintenant ils sont plus contre
tout le système au complet il y a une nouvelle neutralité carbone cette
stratégie qui va plutôt dans leur sens et qui va peut-être éventuellement
leur facilité la tâche peut-être j'espère de deux petits points le premier
point c'est très simple on parle d'environnement je pense que ici on est
tous convaincu savoir ce que ça veut dire mais quand on parle autour de
nous d'environnement je pense qu'une erreur on ne devrait pas il faut qu'on
trouve un autre mot l'environnement c'est je suis quelque peu au centre et
autour de moi il faut que ce soit joli entre guillemets il faut parler de la
vie sur Terre ce matin on parlait des vers de terre voilà c'est ils sont
aussi importants que les humains on est un une espèce vivante parmi des
milliards d'espèces vivantes donc il faut qu'on trouve un vocabulaire qui
fasse consensus mais pas l'environnement parce que ça rejette la nature tous
les problèmes à l'extérieur l'autre point que je voulais soulever c'est
un point positif il y a un procès à Perpignan ou enfin il a été obtenu
gain cause sur le principe de nécessité le principe de nécessité qu'on
recherche souvent ce matin ça a été évoqué également il faut même
aller plus loin que le principe de nécessité c'était donc la nécessité
d'aplatir 3,5 hectares de tournesol qui était des un champ pour faire
des semences de semences ce champ de 3,5 hectares devait permettre dans
ce masser 50% j'ai dit 50% des surfaces tournesol en France donc ça ça
a été neutralisé et grâce à des amis qui sont ultra mobilisés ça a
permis de limiter cette pollution voilà c'est les faucheurs il faut faire
d'OGM merci j'osais pas parce que comme je du tout il y a un guignol qui
avait perdu son téléphone portable au milieu du champ c'était moi qui a
conduit à avoir un procès que les copains ont permis de gagner ils avocats
ont permis de gagner et que 52 comparent en volontaires des gens ils ont dit
voilà il y a un prévenu qui est là et moi je veux être assis à côté
des autres prévenus je veux être s'il y a lieu de condamner je veux être
condamné avec les autres voilà et c'est un sujet les OGM c'est exactement
ce que disait ce qu'on disait ce matin sur l'eau c'est vraiment la même
thématique c'est la confiscation les la gros industriels qui cultivait ces
3,5 hectares c'était juste un mercenaire il est même pas pas du tout voilà
[Applaudissements] on essaie d'alterner un homme une dame il y a la dame et
puis Raphaël il est intervenu oui parce que conférence c'était pourquoi
la politique de l'autruche au sujet du vengeance climatique ce qui m'étonne
c'est que vous pouvez le rapprocher vous avez le micro vous avez par exemple
parler de la [Musique] notre cerveau paresseux faisait qu'on ne luttait pas
contre le changement climatique ou je sais quel autre réseau enfin bref
vous avez dit ah pardon alors excusez-moi je suis vraiment désolé alors
donc je redis le thème c'était donc la politique pourquoi la politique de
l'autruche vous avez par exemple cité le fait qu'on a un cerveau paresseux
des choses comme ça ou vous avez enfin bref je m'attendais quand même à
ce qui est une analyse un peu des enjeux peut-être plus lointain ou plus
bon c'est vrai qu'on est quand même un système comme l'a dit la dame je
suis d'accord avec elle ou un système économique de prédation vous avez
pas parlé de la parce que vous parlez de rénovation énergétique ou de
limitation des des transports ou de transport actuellement qui va être
propre soi-disant alors que c'est basé sur une consommation électrique
qui n'est pas propre puisque si celle nucléaire c'est quand même pas
trop enfin bon bref donc vous n'avez pas parlé d'hyperproductivisme et
de du fait que que c'est un problème énorme aussi et donc en fait tout
est fait pour ne pas mentionner ça pour ne pas penser à ce problème
et je suis un petit peu étonné que vous ayez que ce n'est pas chose là
n'est pas été abordées là moi je suis toujours enfin bon c'est un petit
exemple mais quand on voit actuellement les déchetteries qui sont les gens
inondent les déchetteries de choses qui pouvaient être réutilisables
et rien n'est fait pour utiliser tout ce que les gens jettent abondamment
il y a des tas de choses qui pourraient on pourrait se dire mais c'est ne
rien faire politiquement sur une nation ce ne serait rien de faire en sorte
que des choses soient des objets soient réutilisés au lieu d'être jeté
détruit ou les bouteilles qu'on balance pour les refabriquer bon il y aura
des choses très simples à faire qui ne sont pas faites donc visiblement
il y a des enjeux politiques économiques contrairement c'est une évidence
mais c'est un peu étonné que la conférence n'est pas portée là-dessus
parce que il me semblait que la politique de l'autruche elle n'était pas
seulement basée sur la psyché humaine mais aussi sur sur le fait qu'il y a
des enjeux économiques qui font que quelle évidence ence il y a des discours
apparents qui cachent ces réalités là voilà c'est quand même un petit
peu ouais juste en fait je n'ai pas du tout que des intérêts économiques
c'est évident je parlais vraiment du processus psychologique de l'autruche
c'est à dire pourquoi on rentre en déni d'une certaine chose je parle pas
d'une position explicite vous me faites chier avec votre climat je vais
faire du pognon ça c'est pas la politique de l'autre ça c'est un déni
c'est un refus actif conscient et conscientiser stratégique donc moi ce que
je voulais aborder c'était cette question là de comment on fonctionne sur
cette question là pour plusieurs raisons parce qu'on a perdu la catastrophe
à venir ben on se dit non c'est trop fort j'ai trop d'efforts à faire ça
va être des coûts trop élevés ça va être des coups d'inconfort pour
élever j'ai pas envie de penser ça pour mes enfants en termes d'avenir il
y a 150 bonnes raisons de rentrer dans cette dans cette stratégie de ne
pas regarder mais il y a aussi tout un ensemble d'obstacles qui sont bien
stratégiques conscients je veux pas perdre de pognon je veux en faire plus
sauf que mon dernier les entreprises commencent à réaliser aussi que pour
faire du pognon il faut s'y mettre oui pardon pardon je voudrais quand même
répondre au monsieur là qui avait parce que effectivement je trouve que
vous avez raison la notion la notion d'environnement parler d'environnement
ça permet pas effectivement de bien préciser ce que l'on veut faire et ça
met je dirais la nature et les autres tout ce qui n'est pas humain en dehors
de nous et c'est pour ça que nous avons parlé en grande partie de milieu
de vie la question c'est de savoir le milieu de vie est un milieu de vie qui
est partagé entre des humains et puis des espèces qui sont pas humaines
des animaux des végétaux des bactéries etc et qu'il faut savoir trouver
vos fonds la cohabiter avec ça de façon à rendre la terre plus habitable
voilà c'est ce que je pensais dire alors quand à la deuxième question
que vous pensiez effectivement on en a pas parlé là mais les faucheurs
volontaires depuis qu'ils ont commencé à travailler sur les OGM contre les
OGM etc ils font bien évidemment partie de ces mouvements dans collectif
et positif et politique et donc qui sont à prendre en considération de
notre côté et je pense que le je suis persuadé que il y a un problème de
définition enfin quand ce qu'on entend par politique là le politique bon
qu'est-ce qu'on entend il m'est arrivé plusieurs fois de penser c'était un
peu avant les élections Présidentielles et d'en parler avec des gens de LV
d'ailleurs pour ne pas les nommer que au fond ce qu'ils auraient à faire
c'est soutenir tous ces mouvements et en particulier soutenir et militer
dans par exemple la soulèvement de la terre ou d'autres des trucs comme
ça et que pour moi je pensais que il ferait beaucoup plus avancé la cause
environnementale qu'en pensant aux élections présidentielles ils étaient
pas contents et donc j'étais assez content de leurs résultats je reviens
je reviens un petit peu de la terre c'est même temps de lieu qui a appelé
les secours donc maintenant je crois que ils le font mais je pense qu'il y
a quand même voilà je pense que des élections je pense qu'il ferait mieux
de continuer à soutenir les le soulèvement de la terre plutôt que penser
aux prochaines aux prochaines élections qui seront les européennes qui
comme chacun sait donne plein de députés oui moi je suis très retenir un
petit peu sur justement l'utilisation du mot politique il est certain que le
capitalisme exacerbait que nous connaissons actuellement est une des causes
de cette dégradation de notre situation planétaire ça c'est certain le
deuxième point c'est que il n'y a pas que ce capitalisme là exacerbé et
du maoïsme encore que le maoïsme et conduit à une politique capitaliste
exacerbée elle aussi mais je crois que il faut poser le problème du climat
et de ses conséquences en termes politiques c'est-à-dire comment peut-on
accepter que depuis la crise depuis le covid en particulier mais ça avait
commencé avant les inégalités s'accroissent d'une manière phénoménale
et dans le monde et en France et moi je suis vraiment inquiet quand on
dit en 2000 puisque vous avez dit 35 on sera en équilibre neutralité
carbone je crains que ce soit une inégalité très très fin un objectif
qui entrainera des inégalités encore plus fortes qu'aujourd'hui parce que
les riches avec leur moyen entreprise et autres banques entreprises et vont
savoir trouver des moyens qui vont contribuer à cet équilibre carbone au
détriment des de la population qu'il subira des contraintes de plus en plus
fortes donc moi je pense que il faut poser ce problème en termes politiques
et de politique concrètes précis c'est à dire que pouvons-nous faire quel
type d'union quel type de solution de programme peut-on développer pour
justement lutter contre cette dégradation climatique [Applaudissements]
alors vous avez raison mais je sais pas comment je suis assez d'accord avec
vous je plutôt d'accord avec vous parce que si vous voulez on vit dans
une dans une vision des choses où il y a d'un côté les comment dire le
corps de ceux qui sont conscients écologiquement qui veulent faire des
choses et puis aussi de l'autre côté les capitalistes pour aller vite
sont climato-sceptique Trump et compagnie or il y a un livre qui est paru
récemment de d'Édouard Morena qui s'appelle fin du monde et petit four
qui montre comment il y a comme vous dites une politique menée bon par
plutôt le capital financier et qui prend le tournant écologique mais qui
le prend c'est à dire qui qui est prêt à investir dans à décarboner la
finance comme on dit etc bon et qui a eu un énorme pouvoir en particulier
sur les cops bon et que de ce point de vue là il y a des au sein de ceux
qui sont la conscience écologique c'est pour ça que c'est c'est de moins
en moins une référence la conscience écologique il y a en fait plusieurs
politiques qui s'affrontent et il y a des politiques écologiques comme on
aurait dit autrefois du grand capital de la finance et que donc de ce point
de vue là il suffit pas d'être conscient de ce qui se passe pour lutter et
articuler justice sociale et lutte contre changement climatique là je suis
tout à fait d'accord bon alors moi je suis pas dirigeante de partie je vais
pas vous élaborer un programme vous non plus sans doute mais ce que vous
dites me paraît tout à fait juste on a que trop tendance à se dire bon
tous ceux qui sont conscients de la de la des dégradations qu'entraîne le
dérèglement climatique c'est à même quand non c'est diviseur de il y a
deux personnes il y a deux personnes qui lèvent la main depuis longtemps
et à ce monsieur là-bas j'ai une question pour le sociologue c'est pour
revenir des choses à ce moment matériel tout à l'heure on disait fin
fin du mois et fin du monde donc la personne qui est dans la survie ou dans
l'objectif de du quotidien elle ne peut pas se projeter sur la fin du monde
et c'est pas encadable c'est évident j'ai eu une discussion tout à l'heure
avec les gens qui se battent par rapport à Beauregard enfin aux histoires
de bassines et du projet de ski et en fait ce qui est très intéressant donc
à La Clusaz c'est le le ils appellent ça un Clusaz nostra c'est à dire que
personne ose parler et donc en fait il y a des décideurs le maire et puis les
tâches du dessus au département etc qui ont défini un projet et les gens
locaux sont manifestement semble-t-il thérosés ne veulent surtout pas donc
il y a deux trois enfin il y a ultra minorité qui a mis les pieds dans le
plat en disant c'est pas logique de prendre de l'eau là de faire ça l'eau
va être polluée enfin bon des raisonnements techniques et logiques mais
les choses se font les choses se font parce que un certain nombre d'élus
a été cité tout à l'heure avec Sadie et compagnie un certain nombre
d'élus prennent des décisions et ma question c'est comment se fait-il que
la population ne réagit pas en dehors de quelques extrémistes courageux
ou suicidaires comme on veut comment se fait-il que les autres ne suivent
pas et alors l'information que je vous donne c'est que ils sont allés voir
ils ont essayé d'aller rencontrer les autres la plupart ne veulent même
pas se prononcer et les quelques-uns avec qui ils ont pu échanger qui sont
des agriculteurs ont commencé dans le discours à dire non mais l'eau on
a besoin pour nos bêtes ceci cela et puis au bout d'une d'un certain temps
enfin assez rapidement ils sont convenus que d'un point de vue technique non
ils en ont pas besoin et que et que en fait il ne s'opposait pas au maire
pour des questions purement psychologiques de peur c'est à dire que si je
m'oppose au maire alors j'aurais pas l'autorisation de faire cette truc ou de
construire-t-elle bien ou de des pluies donc on est au niveau mafia au niveau
zéro donc la question parce que en fait quand je les quelques-uns qui se
positionnent qui osent parler grosso modo tous ont un raisonnement basique
dans oui c'est vrai c'est absurde de faire ça donc je suis d'accord avec
toi mais j'ose pas le dire alors la question c'est comment faire d'un point
de vue psychologique sociologique je sais pas pour que cette majorité de
gens qui sont pas foncièrement opposés mais qui n'osent pas comment faire
qu'il bascule qui se rendent compte qu'en fait et que les quelques-uns qui
font des des projets délirants des éléphants sont minoritaires et sont
défendent l'intérêt d'une ultra minorité voilà donc c'est l'histoire
de la démocratie tout simplement donc je vais vous résoudre ça en une
minute non en fait je n'ai pas besoin d'une grande majorité vous avez juste
besoin d'une minorité suffisantement vocale pour faire pour faire bouger les
choses là où il y a un problème c'est qu'effectivement dans les enquêtes
grandes populations la plupart des gens mais plus de 80 % disent non changement
climatique c'est une c'est un vrai problème mais là aujourd'hui il faut que
j'aille chercher mes enfants là aujourd'hui normalement je sais même pas
si je vais avoir des vacances moi cet été et en fait moi je suis sûr que
si je faisais une enquête un peu relativement correctement foutue auprès
de cette population là c'est pas que les gens se racontent c'est qu'ils
ont autre chose à faire trop de change mentale c'est un nombre de trucs
puis franchement ça m'interpelle pas directement et ensuite il y aura une
partie qui aurait peur justement pour des questions de conflits entre ce
que j'ai envie de faire et ce que le maire me peut me permettre ou pas de
faire des choses comme ça mais sur la majorité ça m'a empêchera pas de me
prendre une douche le soir la question de l'eau donc si vous commencez par
les piscines dans le sud peut-être là ça sent pas les mêmes réponses
et donc c'est pour ça que ça m'étonne pas d'avoir ça en fait une petite
partie qui a peur c'est celle qui sera probablement la plus impliquée puis
une grande partie qui en fait à autre chose à faire c'est du ça c'est
la partie un peu décourageant je dois dire parfois parce que le pouvoir
est le gouvernement Macron maintenant il est conscient de ce que vous dites
là que les mouvements citoyens comme les soulèvements de la terre et tout
ça ils ont un point énorme et ils en ont très peur et c'est justement
c'est le langage qui est utilisé et il faut absolument se battre contre ces
termes d'écoute terrorisme et le fait qu'ils amalgamment aussi le le comment
l'extrême gauche et l'extrême droite voilà parce qu'à partir de là dans
les médias prégnants on va les écho terroristes ceux qui se battent sur le
terrain par le citoyen lambda il va être assimilé à djihadiste voilà donc
c'est à nous aussi dans nos luttes de lutter contre ça quoi d'expliquer et
de communiquer sur ce fait là et aussi et aussi et aussi de lutter contre
Macron qui qui essaye de sortir enfin qui dénonce et de sortir du champ
républicain tout ce qui le gène en fait c'est le début de la dictature
en somme parce que tout opposant n'est plus démocratique la lutte contre
les droits de l'homme etc les syndicats les soulèvements de la terre etc
non mais tout à fait ça montre le caractère politique de la chose et que
c'est sur le plan politique que ça joue et que au lieu de d'élargir le
débat démocratique on on le gouvernement se concentre sur un petit noyau
technique et tout ce qui est opposant est renvoyé aux extrêmes donc il y
a une lutte politique à faire ça on est entièrement compliqué allô oui
alors ben merci le molude politique a été prononcé bon Emmanuel Macron
n'est que la version française je crois qu'on a peut-être quand même pas
tout à fait assez dit mais ça et certain ont tenté de le faire que c'est
une lutte mondiale bien évidemment que quand même le capitalisme fossile
c'est une réalité qui est quand même décrite à l'université aussi par
beaucoup de nos collègues aujourd'hui il faut quand même dire que voilà
Emmanuel Macron fait le jeu d'un certain nombre de grands intérêts que
la lutte va être extrêmement difficile que le sainte Soline c'est que le
tout début de malheureusement ce vers quoi on peut craindre qui va falloir
aller combien même beaucoup de mouvements citoyens se déclenchent il faut
quand même savoir contre qui on s'affrontent s'affrontent les gens qui ont
créé l'industrie pétrolière les les multinationales qui la maîtrisent
aujourd'hui bon on a l'exemple de Total en France ne vont pas s'arrêter
comme cela et ils ont des moyens d'action des moyens de pression qui sont au
niveau de nos gouvernements qui sont au niveau à des niveaux extrêmement
multiples il est très important qu'une prise de conscience mais croire qu'on
pourra échapper une lutte politique à des actions directes et barbercier
les chevaux cher volontaires d'avoir dit ici ce qu'est l'action directe
aussi qu'on vient elle est indispensable malheureusement mais il faut savoir
qu'elle va avoir un coup qui va être de plus en plus lourd voilà moi je
veux pas être pessimiste par contre je voudrais quand même aussi dire
quelque chose de plus optimiste on a peu parlé d'énergie en tant que telle
ici c'est quand même paradoxal je dois dire je veux dire que les énergies
renouvelables tous les spécialistes le savent peuvent fournir l'énergie pour
la planète entière qu'aujourd'hui les solutions technologiques sont là
sont disponibles elles ne sont pas sans effet environnementaux certes mais
elles n'ont rien à voir avec ce qu'est le fossile ceci tout le monde doit
le savoir il suffit d'écouter les leçons au Collège de France de Daniel
lako le photovoltaïque l'éolien tous les spécialistes disent ces solutions
ont atteint un optimum technologique pour quelle personne ne croyait il y a
encore quelques années pourquoi ne sont-elles pas mises en place pourquoi
seul le nucléaire est proposé comme une alternative et va entraîner la
France dans le mur bon C'est ça la vraie question politique pour la France
mais au niveau mondial tout le basculement vers les énergies renouvelables
est engagée regardez lisez autre chose qu'en français moi c'est juste ce
que j'ai à dire lisez en anglais vous verrez ce que disent les Anglo-Saxons
les Américains toutes les lettres d'information aujourd'hui bon c'est un
mouvement mondial la Chine elle-même est à la tête de cet équipement
je crois qu'en France on est maintenu sous un oui la Chine et le pays qui
rejette le plus de fossiles mais elle est aussi de loin celui qui installe
le plus de renouvelables tout est à la dimension de la Chine il faut quand
même que aujourd'hui en France enfin moi je crois on sorte de ça parce
que on est maintenu sous un boisseau en fait médiatique un boisseau de
non-information qui est absolument effrayant voilà je crois que Stéphane
moi je connais depuis 30 ans voilà il est venu à Grenoble quand j'étais
étudiant je sais et je dois lui dire qu'à l'époque il était le seul à
parler de climat et que nous étions de jeunes doctorants à Grenoble et
que nous aimions Stéphane mais que ça ne nous touchait pas il faut dire la
vérité nous suggène n'était pas là-dessus nous ne suis pas intéressé
pas du tout nous étions tous à dire oui Stéphane c'était le Québécois
qui débarquait il était le premier à parler beaucoup de climats non
mais Stéphane c'est pas des histoires tu le sais et voilà et voilà moi
je dirais hommage aujourd'hui et c'est et merci quand même à te voilà
[Applaudissements]
