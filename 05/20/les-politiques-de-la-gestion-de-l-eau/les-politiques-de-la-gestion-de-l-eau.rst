.. index::
   pair: Gestion; Eau
   ! Eau

.. _eau_2023_05_20:

====================================================================================================================
|important| ✊🏼✊🏽✊🏾✊🏿 **Samedi 20 mai 2023 10h00, Tom Morel : Les politiques de la gestion de l’eau**
====================================================================================================================


Bilan des impasses actuelles- les choix indispensables pour l’avenir, avec
=============================================================================

Bilan des impasses actuelles- les choix indispensables pour l’avenir, avec:

- Julie Trottier ;
- Nicolas Girod ;
- Valérie Paumier ;
- Julien Le Guet


Pour Julien Le Guet (@BassinesNon), des mégas bassines du marais poitevin aux neiges artificielles de Haute-Savoie, c'est le même combat
==========================================================================================================================================

- https://nitter.net/LeMediaTV/status/1660270675246870528#m

Pour Julien Le Guet (@BassinesNon), des mégas bassines du marais poitevin
aux neiges artificielles de Haute-Savoie, c'est le même combat.

@CRHA_glieres

📹🎤 @Cemil Sanli ⤵️

.. figure:: images/julien_le_guet.png
   :align: center
