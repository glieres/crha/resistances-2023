.. index::
   ! l'imaginaire Face à l’extrême droite : refaire la gauche et son imaginaire, radicalement par Philippe Corcuff
   ! Philippe Corcuff


.. _crha_2023_05_20:

=========================================================================================================================================================
|crha| **Samedi 20 mai 2023 **Rassemblement des Glières à Thorens-Glières**
=========================================================================================================================================================

- https://citoyens-resistants.fr/spip.php?article659
- https://citoyens-resistants.fr/IMG/pdf/tract_recto_verso_impression_crha_2023.pdf
- https://citoyens-resistants.fr/IMG/pdf/crha_programme_mai_2023.pdf

.. figure:: ../19/images/carte_thorens_glieres.png
   :align: center

   Thorens-Glières, cinéma Le Parnal, monument en mémoire des résistants espagnols, MJC et salle Tom Morel https://www.openstreetmap.org/#map=18/45.99726/6.24842


Samedi 20 mai 2023 toute la journéee à la MJC **Photos "enfant perdue de la psyché"**
=======================================================================================

Photos "enfant perdue de la psyché".


Samedi 20 mai 2023 **14h00, MJC : Extrême droite et fascisation de la société, de quoi parle t-on, comment lutter ?**
=================================================================================================================================

Extrême droite et fascisation de la société, de quoi parle t-on, comment lutter ?


Avec :

- Sophie Djigo
- Ugo Palheta
- La Horde

Liens 2024 concernant l'imaginaire Face à l’extrême droite : refaire la gauche et son imaginaire, radicalement par Philippe Corcuff
-----------------------------------------------------------------------------------------------------------------------------------------

- :ref:`raar_2024:corcuff_2024_04_29`


Samedi 20 mai 2023  **14h00, Chapiteau :Palestine, apartheid et récit interdit**
===================================================================================

Palestine, apartheid et récit interdit avec Salah Hamouri ; Anne Tuaillon



Samedi 20 mai 2023 **20h30  Salle Tom Morel Soirée festive engagée avec les Valientes Gracias**
===================================================================================================

- https://vimeo.com/795545511?embedded=true&source=vimeo_logo&owner=5215144

Le groupe Valientes Gracias propose une cuisine musicale aux épices
féministes, qui puise dans les rythmes afro-colombiens pour chanter les
luttes pour l’égalité et dénoncer les oppressions et les dominations.

Dans un univers scénique ou la présence des femmes reste minime, dans
cette société dont la perte de sens et de perspectives s’accélère, la
musique festive et les paroles du groupe font du bien aux corps, aux cœurs
et aux esprits.
