.. index::
   pair: Soutien; Femme Vie Liberté
   pair: Soutien; Femme Vie Liberté
   ! Soutien au peuple Iranien
   ! Iran
   ! MasahAmini

.. _femme_vie_liberte_2023_05_20:

======================================================================================================================================================================
|JinaAmini| ❤️ **Samedi 20 mai 2023 toute la journéee à la MJC Exposition "Femme, Vie, Liberté" en soutien au peuple iranien** #FemmeVieLiberté #MasahAmini ♀️✊ 📣
======================================================================================================================================================================

- https://www.instagram.com/iranluttes/
- https://iran.frama.io/luttes/index.html
- https://iran.frama.io/exposition/
- :ref:`iran_luttes:iran_luttes`


En soutien au peuple iranien. Sélection d’affiches issues du site https://www.macval.fr/Femme-Vie-Liberte

**Merci au CRHA ❤️ et au musée https://www.macval.fr (Val de Marne) !**



- 🏴󠁩󠁲󠀱󠀶 Jin Jîyan, Azadî #JinJiyanAzadî (kurde, 🏴󠁩󠁲󠀱󠀶 )
- 🇮🇷 Zan, زن, Zendegi, زندگی, Âzâdi, آزادی (Persan) #ZanZendegiÂzâdi 🇮🇷
- 🇫🇷 Femme, Vie, Liberté #FemmeVieLiberte (Français, 🇫🇷)
- 🇮🇹 Donna, Vita, Libertà (Italien, 🇮🇹 )
- 🇬🇧 Woman, Life, Freedom #WomanLifeFreedom (Anglais, 🇬🇧 )
- 🇩🇪 Frau, Leben Freiheit (Allemand, 🇩🇪)



.. figure:: images/vue_d_ensemble_2.png
   :align: center


.. figure:: images/vue_d_ensemble_3.png
   :align: center

1) Femme, Vie Liberté ,artwork by Marjillu
============================================

.. figure:: images/femme_vie_liberte_oiseaux.png
   :align: center

   Artwork by Marjillu


.. figure:: images/vue_d_ensemble_3.png
   :align: center

2) Womens rights are human rights, artwork by Mina Ja Re
==========================================================

.. figure:: images/femme_vie_liberte_womens_rights_are_human_rights.png
   :align: center

   Womens rights are human rights, artwork by Mina Ja Re


.. figure:: images/vue_d_ensemble_3.png
   :align: center

3) Femme, Vie Liberté, artwork by M. Melgrati
========================================================

- https://marcomelgrati.com/

.. figure:: images/femme_vie_liberte_melgrati.png
   :align: center

   Artwork by M. Melgrati

.. figure:: images/vue_d_ensemble_3.png
   :align: center


4) **Hey fascist preacher, leave them kids alone**, artwork by Legomahi
=========================================================================

.. figure:: images/femme_vie_liberte_legomahi.png
   :align: center

   Artwork by Legomahi

.. figure:: images/vue_d_ensemble_3.png
   :align: center

5) Femme, Vie Liberté, artwork by Oudemir
========================================================

.. figure:: images/femme_vie_liberte_oudemir.png
   :align: center

   Artwork by Oudemir


.. figure:: images/vue_d_ensemble_3.png
   :align: center

6) Women of Iran artwork by Shaboolim
========================================================

.. figure:: images/femme_vie_liberte_shaboolim.png
   :align: center

   Women of Iran , Artwork by Shaboolim


.. figure:: images/vue_d_ensemble_3.png
   :align: center

7) My Hair is not your politics, artwork by Dosephs
========================================================

.. figure:: images/femme_vie_liberte_dosephs.png
   :align: center

   My Hair is not your politics, Artwork by Dosephs


.. figure:: images/vue_d_ensemble_3.png
   :align: center


8) Femme, Vie Liberté, artwork by Yael Hofri
========================================================

.. figure:: images/femme_vie_liberte_yaelhofri.png
   :align: center

   Artwork by Yael Hofri

.. figure:: images/vue_d_ensemble_3.png
   :align: center

9) Femme, Vie Liberté, artwork by Eric Giriat
========================================================

.. figure:: images/femme_vie_liberte_eric_giriat.png
   :align: center

   Artwork by Eric Giriat


.. figure:: images/vue_d_ensemble_3.png
   :align: center

10) **Hijab is not a choice until it is a choice for all women**, artwork by FROM__IRAN
=========================================================================================

.. figure:: images/femme_vie_liberte_from_iran.png
   :align: center

   Hijab is not a choice until it is a choice for all women, artwork by FROM__IRAN


.. figure:: images/vue_d_ensemble_3.png
   :align: center

11) Femme, Vie Liberté, artwork by Grafikret
========================================================

.. figure:: images/femme_vie_liberte_grafikret.png
   :align: center

   Artwork by Grafikret


.. figure:: images/vue_d_ensemble_3.png
   :align: center


12) Femme, Vie Liberté, artwork by Innerjalz
========================================================

.. figure:: images/femme_vie_liberte_innerjalz.png
   :align: center

   Artwork by Innerjalz


Liens 2024
============

- :ref:`crha_2024:iran_solidarite_2024_05_11`
