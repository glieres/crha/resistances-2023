.. index::
   ! Eco-féminisme

.. _eco_feminisme_2023_05_20:


==================================================================================================================
|important| ✊🏼✊🏽✊🏾✊🏿 Samedi 20 mai 2023 **14h00, Tom Morel : L’écoféminisme, combat d’aujourd’hui** ♀️
==================================================================================================================

**L’écoféminisme, combat d’aujourd’hui** avec:

- https://piped.kavin.rocks/watch?v=uh8Zuj6F83A (CRHA 2023 L’écoféminisme, combat d’aujourd’hui)
- Sandrine Rousseau (https://nitter.moomoo.me/sandrousseau/rss)
- Corinne Morel Darleux (https://piaille.fr/@cmoreldarleux)
- Pinar Selek  (https://pinarselek.fr/biographie/, https://nitter.cutelab.space/Pinar_Selek/rss)
- Camille Bajeux


.. warning:: la prise de son ou vidéo était interdite


.. figure:: images/debut.png
   :align: center


.. figure:: images/debut_lsf.png
   :align: center



.. figure:: images/camille_pinar_corinne_sandrine_plus_une.png
   :align: center

.. figure:: images/pinar_corinne.png
   :align: center


.. figure:: images/camille_pinar_corinne_sandrine.png
   :align: center


.. figure:: images/corinne.png
   :align: center



Vidéo
=======

- https://piped.kavin.rocks/watch?v=uh8Zuj6F83A (CRHA 2023 L’écoféminisme, combat d’aujourd’hui)
- https://piped.video/uh8Zuj6F83A

Pinar Slek
============

- https://nitter.cutelab.space/Pinar_Selek/status/1663116587514068992#m

POUR ARROSER L'ESPOIR, je partage avec nous notre émouvant débat sur
l'écoféminisme comme vecteur de changement social. Avec chères Camille Bajeux,
@cmoreldarleux et @sandrousseau https://piped.kavin.rocks/watch?v=uh8Zuj6F83A (CRHA 2023 L’écoféminisme, combat d’aujourd’hui)

Enorme merci à @JacquesVenjean et au @CRHA_glieres

