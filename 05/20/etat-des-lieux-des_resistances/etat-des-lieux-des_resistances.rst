
======================================================================================================================================================
|important| ✊🏼✊🏽✊🏾✊🏿 Samedi 20 mai 2023 **17h00, Tom Morel : FORUM : État des lieux des Résistances, pour construire demain !** ️✊ ♀️
======================================================================================================================================================


Vidéos
=======

- :ref:`videos_2023_05_20`

Avec la participation de
===========================

Avec la participation de:

- Daniel Coutarel (Confédération Paysanne)
- Julien Le Guet (https://nitter.net/BassinesNon/rss, https://bassinesnonmerci.fr/, https://www.youtube.com/@collectifbassinesnonmerci4941/videos)
- Hamouri Salah (Palestine)
- Hugo Paleta (sur l'antifascisme)
- Julie Trottier ("Le savoir c'est le pouvoir", https://www.theses.fr/069273960)
- Julien Troccaz (https://nitter.net/JTroccaz/rss)
- Nicolas Girod (Confédération paysanne)
- Nadia Salhi (CGT)
- Nina Faure,
- Pinar Selek,
- Sophie Djigo,
- Sandrine Rousseau,
- Yannick Kergoat (réalisateur, https://fr.wikipedia.org/wiki/Yannick_Kergoat)
- etc...

Animé par Gilles Perret (https://uk.unofficialbird.com/Gilles_Perret/rss)


Introduction avec une chanson 🌞 "Jveux du soleil!
=====================================================

- https://nitter.net/LeMediaTV/status/1660278497602830337#m
- https://twitter.com/LeMediaTV/status/1660278497602830337

🌞 "Jveux du soleil!" chante Stéphanie Questana devant une salle débordante
de public, avant que ne s'y expriment les invités de @Gilles_Perret :
Nicolas Le Guet, @Pinar_Selek, Julie Trottier, Hamouri Salah, @sandrousseau,
@Cemil Sanli, La Horde, Ugo Palheta, Nicolas Girod



.. figure:: images/julie_stephanie_cemil.png
   :align: center


.. figure:: images/julie_cemil.png
   :align: center


.. figure:: images/julie_hugo_sophie.png
   :align: center


.. figure:: images/stephanie.png
   :align: center

.. figure:: images/hugo_stephanie.png
   :align: center

