
.. _films_2023_05_21:

==============================================================================
|crha| Dimanche 21 mai 2023 **2 Films au cinéma Le parnal à Thorens Glières**
==============================================================================


.. figure:: ../images/glieres_films_dimanche.png
   :align: center

   https://www.helloasso.com/associations/citoyens-resistants-d-hier-et-d-aujourd-hui/formulaires/1/widget


.. figure:: ../../19/images/carte_thorens_glieres.png
   :align: center

   Thorens-Glières, cinéma Le Parnal, monument en mémoire des résistants espagnols, MJC et salle Tom Morel https://www.openstreetmap.org/#map=18/45.99726/6.24842

Dimanche 21 mai 2023 15h00 au cinéma Le parnal **Eau secours** en présence de Julia Blagny
==============================================================================================

- http://blagny.fr/eau-secours-des-andes-a-lamazonie/
- https://vimeo.com/642722285?embedded=true&source=vimeo_logo&owner=4965346

L'eau est la source de toute forme de vie.

Elle a dessiné la Terre et l'histoire de l'Humanité.

Mais elle peut devenir dangereuse si l'on cherche à contrôler son flux.
Elle peut même devenir poison quand les hommes y déversent mercure, arsenic...

Les rivières de Bolivie, qui sont les veines du cœur de l'Amérique du
Sud, se teignent de rouge, de gris et vont même jusqu'à disparaître.
Les habitants de différents peuples de Bolivie et des spécialistes nous
avertissent : l'accès à une eau potable est chaque année plus difficile.
Des animations et des musiques originales nous entrainent d'un fleuve à
l'autre.

Si nous ne traitons pas l'eau avec respect, nous voguons à notre perte,
en Bolivie comme ailleurs.
Ce documentaire ouvre une fenêtre sur une réalité bolivienne et illustre
une urgence mondiale.
Il peut être un support pour partager nos réflexions et unir nos efforts
pour construire des alternatives, maintenant.

::

    Bolivie/France – 2021 – 52 minutes
    Documentaire de Julia Blagny
    Espagnol – Disponible avec sous-titres en français ou en anglais
    Thématiques : Eau – Environnement – Premières Nations – Droits de l'Homme – Pollution – Engagement
    Musiques originales : Antonio Perez – Marco Antonio Peña
    Dessins – Angèle Charbouillot


Dimanche 21 mai 2023 17h00 au cinéma Le parnal **Reprise en mains** en présence de Gilles Perret
===================================================================================================

- https://jour2fete.com/film/reprise-en-main/
- https://nitter.net/jour2fete/rss
- https://vimeo.com/727929889?embedded=true&source=vimeo_logo&owner=6973199

Avec : Pierre Deladonchamps, Laetitia Dosch, Grégory Montel, Vincent Deniard,
Finnegan Oldfield, Samuel Churin, Marie Denarnaud

Comme son père avant lui, Cédric travaille dans une entreprise de mécanique
de précision en Haute-Savoie.

L’usine doit être de nouveau cédée à un fonds d’investissement.

Epuisés d’avoir à dépendre de spéculateurs cyniques, Cédric et ses amis
d’enfance tentent l’impossible : racheter l’usine en se faisant passer
pour des financiers !


