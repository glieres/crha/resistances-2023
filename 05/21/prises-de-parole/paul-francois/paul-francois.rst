
.. index::
   pair: Résistant; Paul François
   ! Paul François

.. _paul_francois:

=========================================
**Paul François** (contre monsanto)
=========================================


Présentation
=================

Céréalier intoxiqué par un herbicide Monsanto en 2004 qui a obtenu au
final la condamnation de Bayer. Physiquement agressé chez lui début 2023.


.. figure:: images/paul_francois.png
   :align: center

   https://nitter.net/Gilles_Perret/status/1660235771285065729#m


Vidéo
======

- https://piped.video/watch?v=eqK43CTZhck
- https://www.youtube.com/watch?v=eqK43CTZhck
