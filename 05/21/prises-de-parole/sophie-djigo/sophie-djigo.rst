
.. index::
   pair: Résistante; Sophie Djigo
   ! Sophie Djigo

.. _sophie_djigo:

====================================================
**Sophie Djigo** (sur l'accueil des migrants)
====================================================


Présentation
=================

Enseignante de philosophie menacée par l’extrême droite pour son travail
sur les migrations avec ses élèves.


Vidéo de Sophie Djigo
=========================

- https://piped.video/X97hKVYZhJA
- https://www.youtube.com/watch?v=X97hKVYZhJA



Transcription Youtube (à retravailler)
-------------------------------------------


Sophie gigot et philosophe spécialiste des questions migratoires et
fondatrices du collectif migration 59 depuis plusieurs années dans le cadre du
programme de sa classe préparatoire littéraire à Valenciennes elle emmène
ses étudiants à Calais pour aborder les questions d'exil et de citoyenneté
cette année cependant par mesure de sécurité se déplacement a été annulé
par le rectorat car il a déclenché une vague de violence de l'extrême droite
le qualifiant d'endoctrinement au service du Grand remplacement harcèlement
intimidation cette violence est même allée jusqu'au menaces de mort envers
l'enseignante ces attaques présentent une sérieuse menace sur la liberté
d'enseigner [Applaudissements] bonjour à tous j'ai grandi en Lorraine en
Moselle dans une région où la notion de frontières et poreuse plus la
bile que sur d'autres territoires on pourrait se dire que justement c'est aux
frontières que s'affirme le nationalisme le plus pur en particulier celle là
objet dramatiser sacraliser objet de deuil de revanche de guerre rappelons-nous
le slogan martelé il faut retrouver l'Alsace et la Moselle mais c'est tout
le contraire c'est aux frontières qu'on apprend que les hommes sont tous
les mêmes que les cultures et les langues s'hybrides que les appartenances
nationales résulte d'un jeu de bingo politique la résistance avec un r
majuscule j'ai découvert sur les bancs de l'école comme beaucoup d'entre
nous c'est l'une des parties grandioses de notre histoire héroïque c'est
du moins ce que je me disais en suivant les cours d'histoire-géographie et
j'appartiens à une génération qui a connu les survivants qui ont fait la
guerre qui l'ont subi qui ont résisté c'est l'une des questions que j'ai
posées à mon grand-père mon grand-père n'a pas été résistant j'étais un
peu déçu toutefois il avait été enrôlé au STO dans une usine allemande
et une nuit il s'en est évadé à vélo jusqu'au petit village frontalier
de Reyersviller un village au pied des forêts épaisses de conifères là
où la terre est rouge il a trouvé refuge dans une maison on l'a caché
au sous-sol jusqu'à la Libération et là il a épousé la jeune fille
de la maison Marie qui est devenue ma grand-mère une histoire ordinaire
pour l'époque comment hérite-t-on de ce passé mythique avec mon regard
de philosophe je me suis fait plusieurs réflexions je les pose ici pour
en venir ensuite à mon propre engagement et à la manière dont je peux le
penser comme une forme de résistance d'abord je me suis demandé comment on
devient résistant je veux pas dire dans quelles conditions historique pour
quelle motivation etc mais quelles sont les distributeurs de légitimité
d'une époque d'une société qui adoube les résistants qui décident que
tel ou tel mouvement tel ou telle personne peuvent être qualifiées de
résistants plutôt que de terroristes de casseurs de Black Bloc etc c'est
la question de la légitimité de l'action résistance et je vois bien que
si nous sommes réunis tous ici dans des journées comme celles-ci c'est bien
pour concurrencer des autorités suspectes en matière de distribution de la
légitimité ensuite je pense que la légitimité est une affaire d'échelle
je me suis même demandé si parfois l'héroïsation des résistants n'était
pas une stratégie du pouvoir pour écraser les velléités de résistance
parce qu'à trop mythifier les figures de résistance on les éloigne de
nous on s'interdit d'en faire partie ce serait présomptueux d'oser affirmer
qu'on est un résistant ou une résistance et tous ceux qui se livrent à
cet exercice sur le plateau des Glières le savent bien il y a un embarras
à se dire résistant à se comparer à ceux qui ont lutté ici au péril
de leur vie dans les années 40 et je crois fermement qu'il faut lever cette
embarras et assumer cette filiation cet héritage ne pas se laisser écraser
par l'héroïsation la résistance mais c'est une pratique ordinaire et
c'est bien cela qui effrayent les gouvernants exceptionnaliser la résistance
c'est l'affaiblir elle est d'autant plus forte qu'elle est banale quotidienne
parfois anonyme une forme de mobilisation discrète la petite échelle donc
qui est celle des petits pas du quotidien la petite échelle aussi qui
est celle du local nous avons souvent le sentiment d'être impuissant en
matière politique que les leviers nous échappent que les choses se jouent
à l'échelle nationale européenne mondiale et cela nous fait oublier que
le ressort fondamental de la politique c'est l'action citoyenne celle-ci ne
se borne pas au vote elle inclut diverses formes d'action diverses formes de
résistance quand des politiques ne sont pas menées en notre nom quand nous
nous sentons exilés au sein de nos propres états nous pouvons basculer
dans une opposition active la résistance certes les gouvernants ont le
pouvoir mais nous citoyens nous avons la puissance il faut pas minimiser
cette puissance d'action elle méprise la rhétorique politique de l'état
d'urgence qui autorise tout parce qu'elle se déploie dans une temporalité
longue une stratégie d'usure de SAP nous nous ne sommes pas soumis à des
échéances électorales nous avons le temps nous sommes des puissances
d'érosion du pouvoir je voudrais défendre une vision antihéroïque de la
résistance des résistances minuscules j'ai été bibronnée à la lecture
de Robert luzil qui a lui-même du s'exiler en Suisse face aux fascisme et
qui disait ceci à propos de l'esprit du grimpeur qui veut atteindre les
sommets le pied le plus sûr est toujours le plus bas je crois qu'il ne faut
pas craindre l'humilité pas craindre d'officier au bas de l'échelle dans
les larmes du pouvoir pas craindre non plus de porter des coups bas cette
dernière leçon c'est une leçon du féminisme quand les suffragettes
choisissaient le juge dessus plutôt que le noblart de la boxe pour une
autodéfense plus efficace nous nous sommes les petites mains d'en bas qui
résistent avec leur petit moyen et si la vulgarité de notre militance
irrite les grands esprits et bien qu'à cela ne tienne je suis entrée en
résistance par hasard et par nécessité le hasard qui m'a muté dans le
Nord-Pas-de-Calais comme beaucoup de jeunes fonctionnaires à une autre
frontière le hasard qui a fait que pendant 7 ans j'ai pris la ligne de
TER Lille Calais pour aller enseigner à Saint-Omer c'est là que j'ai vu
s'incarner ce qu'on appelle abstraitement le phénomène migratoire non pas
des hordes de migrants au visage inquiétants ça jamais quelques dizaines
d'exilés abordent des trains repérables à leurs chaussures crottées de
la boue des camps jusqu'à ce qu'un jeune érythréen qui Brom me tend d'un
paquet de chips au fond d'un hall de gare obscur à Hazebrouck chez moi
en Lorraine Hazebrouck c'est le nom qu'on utilise pour dire Pétaouchnok
qu'elle ne fût pas ma surprise de découvrir qu'il y avait réellement une
commune qui s'appelait Hazebrouck jusqu'à ce qu'on m'invite dans la jungle
de Calais puis celle de nos rencontres j'ai fait ce que ma mère m'a toujours
appris ne pas détourner le regard ma mère qui m'a transmis son habitous
de travailleur social cette idée qu'il ne faut pas être déconnectée du
réel qu'il ne faut pas ignorer la misère la souffrance mais au contraire
qu'on a le devoir de l'accueillir les yeux dans les yeux question dignité
en philosophe percuté par la réalité de la condition migrante j'ai écrit
un livre puis de puis trois consacrés aux questions migratoires l'écriture
ça peut être une forme de résistance mais celle-là ne m'a pas suffi face
à l'immédiateté de la situation des exilés à Calais il y a eu ce matin
de novembre il avait neigé tôt cette année-là 2017 le coup de fil d'habit
jeune migrant éthiopien expliquant que la police a raflé toutes les tentes
toutes les couvertures Abby qui me dit qu'il va marcher toute la nuit pour
ne pas mourir de froid et moi le lendemain matin qui roule vers Calais pour
aller le récupérer et nous qui l'invitons à venir prendre un peu de répit
à la maison je dis nous et c'est important parce que la résistance ne peut
être que collective nous c'est d'abord ma famille happée par le réel par
la nécessité au bout d'une semaine ils étaient cinq à camper dans le salon
puis on a demandé à nos amis s'il pouvait accueillir leurs amis et le fil en
aiguille nous avons créé le collectif migration 59 collectif de résistance
citoyenne qui offre un accueil de répit aux exilés en transit bloqué à
la frontière face à la violence d'État inouïe qui règne à Calais nous
avons cherché un moyen de résister non violent solidaire et constructif
un mode de résistance qui ne soit pas seulement négatif et réactif mais
qui dessine aussi modestement les lignes de la société cosmopolite est
plus juste à laquelle nous rêvons notre slogan est devenu accueillir c'est
résister résister en ouvrant sa porte en offrant de la chaleur humaine de
la considération un toit et je réalise aujourd'hui combien jérites non
pas de l'évasion somme tout assez héroïque de mon grand-père mais de
l'action anonyme et discrète de cette grand-mère que je n'ai jamais connue
elle aussi a su accueillir dans la clandestinité même celui qui cherchait
refuge aujourd'hui notre maison est devenue le refuge de centaines d'exilés
fuyant l'oppression en Erythrée les massacres en Éthiopie la guerre du
Darfour et du corps dauphin du Sud quand les exilés viennent chercher du
répit auprès de migration ils disent qu'ils viennent pour les families
la pratique d'hospitalité que nous avons développée relève pour eux
d'une construction familiale de ce rêve d'une fraternité élargie d'une
famille universelle c'est aussi une pratique intéressante dans un contexte
où la violence d'État cible particulièrement les militants et tous les
partisans de l'égalité et de la justice sociale parce que cette pratique
déplace le terrain de lutte de l'espace public l'Agora traditionnellement
réservé aux seuls hommes libres dans le monde grec à la Domus la sphère
domestique privée espace dévolu aux femmes avec migration le privé devient
politique en écho au fameux slogan féministe des années 70 les tâches
subalternes dédiées aux femmes prépare une chambre cuisinée deviennent
des gestes politiques des actes de résistance lorsqu'il s'adresse à
des migrants clandestins un pied de nez aux politiques de fermeture et
du Zéro migrant alors certes nous résistants sont minuscules et on peut
regretter leurs caractères dérisoire mais je pense que c'est justement
la bonne échelle parce que les gouvernants ne peuvent pour l'instant rien
sert contre ses actions minuscules qui se déploient selon une logique
de l'essence partout sur le territoire dans le Nord le Pas-de-Calais chez
nos voisins belges à Paris à Nantes dans la Roya à Bayonne la liste est
tellement longue absolument partout des citoyens accueillent ils font ce que
les politiques pensent impossible ils vivent ensemble avec des étrangers
il réalise ce que les politiques n'osent même pas imaginer ils rendent
l'utopie ordinaire certes nos résistances sont humbles et discrètes mais
pas tant que ça puisqu'il suffit d'une visite d'études organisée à Calais
par un professeur avec ses étudiants d'hippocaques pour que les militants
de reconquête se déchaînent pour que la fachosphère s'embrase force est
de constater que notre seul présence à la puissance d'être insupportable
pour les fascistes on peut au moins se targuer de cela et ce que j'ai compris
quand le flot de haine est tombé dessus en novembre dernier c'est combien
ils ont peur l'essence du fascisme je crois que c'est cela la peur donc il
faut nous rappeler de quel côté et la peur ce n'est pas à nous de vivre
dans la crainte ce sont eux qui ont peur de nous de nos idées de nos actions
de nos modes de vie de la société que nous incarnons déjà parce que certes
nous sommes infimes mais imaginez ce que serait le monde sans nous sans ces
innombrables acteurs de la société civile engagée dans les luttes sociales
dans l'autodécence des plus vulnérables dans les luttes écologiques etc
etc en tant que philosophe et professeur de philosophie je comprends à quel
point je suis à la bonne place à quel point mon simple métier d'enseignante
et cruciale lorsqu'il s'agit de défendre la vérité contre les attaques
et les renversements permanents des fascistes ils craignent les savoirs ils
craignent les faits qui fragilisent leurs opinions fallacieuses amener des
étudiants sur le terrain de la frontière a mis le feu aux poudres parce que
le fascisme se nourrit du mythe et qu'il ne supporte pas la confrontation
avec le réel j'ai conclurai ainsi en disant que pour moi la résistance
c'est d'abord cela avoir le sens des réalités aller sur le terrain ne pas
fermer les yeux bref être du côté du réel merci [Applaudissements]
