
.. index::
   pair: Résistante; Angélique Huguin
   ! Angélique Huguin

.. _angelique_huguin:

===========================================================================
**Angélique Huguin** (sur l'enfouissement des déchets nucléaires)
===========================================================================


Présentation
=================

Opposée au projet d’enfouissement de déchets nucléaires à Bure, en Meuse,
elle fait partie des militants mis en examen pour « Association de malfaiteurs ».


.. figure:: images/angelique.png
   :align: center


.. figure:: images/angelique_julien.png
   :align: center



Vidéo
=========

- https://piped.video/-biMlT-oUtc
- https://www.youtube.com/watch?v=-biMlT-oUtc


