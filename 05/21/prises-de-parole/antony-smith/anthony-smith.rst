
.. index::
   pair: Inspecteur du travail; Anthony Smith
   pair: Inspecteur du travail; Pierre Lamy
   pair: Résistant; Anthony Smith

.. _anthony_smith:

=========================================
**Anthony Smith évoque Pierre Lamy**
=========================================


Présentation
=================

Inspecteur du travail dont la sanction disciplinaire a été annulée par
le tribunal de Nancy en octobre 2022.



Lancement des Paroles de Résistances avec l’inspecteur de travail Anthony Smith ⁦au Plateau des Glières
========================================================================================================

- https://nitter.net/Gilles_Perret/status/1660216037181194243#m

Lancement des Paroles de Résistances avec l’inspecteur de travail
Anthony Smith ⁦au Plateau des Glières devant 1500 personés.

Grandeur et émotion au rdv.
⁦
@blast_france⁩ ⁦@Politis_fr⁩ ⁦@Fakir_⁩ ⁦@ledauphine⁩


🔴Unis, déterminés, conscients et agissants : nous résistons !
==================================================================

- https://nitter.net/smith51_a/status/1660302307567738883#m

.. figure:: images/tweet_2023_05_21.png
   :align: center

.. figure:: images/anthony_smith.png
   :align: center

🔴Unis, déterminés, conscients et agissants : nous résistons !

✊Aux Glières, j’ai évoqué l’engagement de `Pierre Lamy, Inspecteur du travail, dans la résistance <https://fr.wikipedia.org/wiki/Pierre_Lamy_(r%C3%A9sistant)>`_

💥Dans ce présent trouble où les régressions sociales s’ajoutent aux
relents fascistes, défendons le bien commun !

Pierre Lamy
==============

- https://fr.wikipedia.org/wiki/Pierre_Lamy_(r%C3%A9sistant)

Après l'armistice, Pierre Lamy reprend son poste à la préfecture d'Annecy.
Il dispose désormais d'une secrétaire et il est assisté d'un contrôleur
de la main-d'œuvre. Il renoue avec les syndicats et notamment avec Paul
Viret, responsable CFTC, qu'il convainc de rester à son poste de coordination
des syndicats, malgré leur opposition commune à la politique de Vichy.

Ensemble ils orchestrent l'échec de la manifestation du 1er mai 1941,
censée célébrer désormais le premier terme de la devise travail - famille - patrie, non plus
dans l'esprit détestable de la lutte des classes, mais dans celui, radieux,
de la concorde qui sera mise en œuvre par la Charte du travail alors en
préparation (loi du 4 octobre 1941).


Vidéo
===========

- https://piped.video/u1QbAUEH4Lk
- https://www.youtube.com/watch?v=u1QbAUEH4Lk


Transcription Youtube (à retravailler)
----------------------------------------------

Anthony Smith responsable syndical au ministère du Travail est inspecteur
du travail sur le terrain depuis plus de 15 ans au début de la première
vague de covid-19 en mars 2020 il demande dans le cas de l'exercice de ces
missions des mesures de protection de la santé dont la mise à disposition
de masque pour des salariés d'une association d'aide à domicile à Reims
où il exerçait Anthony subira de multiples pressions politiques patronales
et interne pour qu'il arrête son action de contrôle il résiste et dépose
le 15 avril 2020 référé devant le tribunal judiciaire de Reims quelques
heures plus tard il sera suspendu de ses fonctions par Muriel Pénicaud alors
ministre du Travail 4 mois plus tard il est sanctionné par Elisabeth borne
devenu à son tour ministre du Travail muté d'office dans la Meuse pendant
deux ans il contestera cette sanction devant le tribunal bénéficiant d'un
large soutien de ses collègues de la gauche social et politique ainsi que
de l'opinion publique Anthony verra cette dernière totalement annulée en
octobre 2022 il a réintégré son poste au 1er janvier 2023 bonjour bonjour à
toutes et tous merci merci je suis très très heureux très honoré d'être
parmi vous aujourd'hui je vais bien évidemment vous raconter brièvement
la drôle d'histoire que j'ai eu à subir je fais des guillemets dans le
terme drôle mais avant je voulais vous dire quelques mots de l'inspection
du travail de ces inspectrices de ces inspecteurs qui au quotidien tant de
veiller à l'application de la réglementation du travail alors qu'ils ne
sont plus que 1750 dans le pays pour 20 millions de salariés et près de 2
millions d'entreprises vous parlez de ceux qu'on a historiquesment appelé les
Voltigeurs de la République dont la naissance est un train sec banlié à la
question sociale à la protection de la partie faible du contrat de travail
le salarié c'est en effet dans une des premières lois du travail celle
du 2 novembre 1892 sur le travail des enfants des filles et des femmes dans
les établissements industriels qui est décidé la création d'inspecteur
du travail chargé de veiller à l'application des quelques lois sociales
alors en vigueur et dans cette longue histoire de l'inspection du travail
qui préexiste même à la création du ministère du Travail en 1906 il y a
des périodes glorieuses et d'autres honteux période glorieuses comme celle
où les inspecteurs inspectrices veillèrent à la mise en œuvre dans les
entreprises du conquis social de la journée de 8h à partir de la loi d'avril
1919 histoire heureuse de ces nouvelles générations d'inspecteurs consciente
et agissantes qui depuis le tournant du siècle vont porter le combat pour
la défense d'un code du travail protecteur des droits des plus faibles on
les retrouvera résistant aux volonté du ministère de l'Intérieur de se
servir d'eux qui disposent d'un droit d'entrée de jours comme de nuit dans
toute entreprise où sont susceptibles d'être occupés des salariés pour
les utiliser aux opérations de chasse et de reconduites des étrangers sans
titre aux frontières c'est l'Organisation internationale du Travail l'OIT
qui viendra en a plus aux inspecteurs et à leur organisation syndicale pour
dénoncer les pratiques du gouvernement français comme contraire au texte
internationaux qui les régissent on les retrouvera à ces inspectrices et ses
inspecteurs je suis ému de le dire ici lorsque 1300 d'entre eux signeront
une tribune publique de soutien pour dénoncer les sanctions disciplinaires
dont j'ai fait l'objet mais il lieu aussi des périodes honteuses je vous
l'ai dit l'inspection du travail et intimement lié à la question sociale
aux lois sociales mais également aux lois antisociales comment au Glières
ne pas évoquer le gouvernement de Vichy qui fera des inspecteurs du travail
les dossiers manœuvres de la politique de collaboration mais je n'ai pas
envie de vous parler de l'inspection qui collabora je vous parlais de celle
qui résista et parmi elles de Pierre Lamy né en 1909 il entre à l'école
en 1900 l'École normale en 26 et enseignant ensuite en 37 l'ami passe le
concours départemental d'inspecteur du travail organisé par le gouvernement
du Front populaire il l'obtient choisit la Haute-Savoie il est affecté à
Annecy intègre son bataillon en 39 reprend son poste après l'armistice
rejoint la résistance des 41 en avril 42 le gouvernement lava organise la
politique dite de la relève c'est-à-dire le départ volontaire d'ouvriers
vers l'Allemagne en échange de la libération de prisonniers face à l'échec
du volontariat la réquisition des travailleurs se met en place suivi du
service du travail obligatoire les inspecteurs du travail sont chargés
en échange d'une prime d'établir les listes de travailleurs pierre l'ami
refuse résiste de multiples façons dans les services de l'inspection et en
dehors d'autres murs racontaient que moi son histoire héroïque arrêter le
28 juin 44 il sera exécuté le 18 juillet 44 dans un bois au col de lait
chaud je viens de Reims et je vous dis cela car le bâtiment historique
de l'inspection du travail à Reims lorsque j'ai débuté dans le métier
était occupé depuis bien avant la guerre par l'inspection du travail et
au grenier se trouvaient les archives de ces fameuses fiches du STO lorsque
les services de l'inspection ont déménagé je me suis battu avec mes
collègues pour que ses filles ses rapports ses instructions ne soient pas
jetées à la poubelle nous avons obtenu que les archives départementales
viennent pendant plusieurs mois les prélever et constitue un fond qui ne
demande plus qu'à être étudié à vie aux historiennes et aux historiens
c'est à Pierre Lamy à cet esprit de résistance qui je le sais nous anime
toutes et tous ici que je pense aujourd'hui ma drôle d'histoire et sans
commune mesure mais à les révélatrice de fonctionnement d'Évian de
l'État qui interroge sur l'état de droit dans lequel nous nous trouvons
gardez en tête avant que je n'en dise de mots que les inspecteurs du travail
bénéficient d'une large indépendance garantie par la Convention numéro
81 de l'Organisation internationale du travail de 1947 qui leur permet de
diligenter leur contrôle et leurs enquêtes dans les entreprises nous sommes
mi mars 2020 et le premier confinement fils la France l'on découvre alors
les invisibles de la République ces essentiels que l'on appellera plus tard
les premiers de corvées livreur des plateformes chauffeurs de VTC agent
de sécurité caissières caissière aide à domicile à Reims où j'exerce
ce sont justement les représentants du personnel d'une association d'aide
à domicile qui m'a l'air sur leurs conditions de travail des salariés ou
contractés covid sont hospitalisés des usagers de la structure en sont
décédé les protections dont des masques manquent l'organisation du travail
et défaillantes j'enquête du moins GC dans un contexte où le ministère du
Travail est volontairement mis à l'arrêt sous la coupe du ministère de la
Santé et des prescriptions des autorités sanitaires où notre hiérarchie
rend quasiment impossible la conduite de tout contrôle ou le droit et son
application semble chaque jour s'effacer devant les raisons de l'État je
viens d'apprendre qu'à Lille une collègue inspectrice du travail a réussi
à obtenir d'un juge indépendant des mesures de protection de la santé des
travailleurs dans une association d'aide à domicile je me dis que c'est le
moyen pertinent et légal d'imposer à l'employeur des mesures qui ne prend
pas nous sommes le week-end de Pâques 2020 le 11 avril je m'en souviens
si bien je rédige l'assignation en référé tout en informant l'employeur
de ma décision quelques heures plus tard il est plus de 21h je reçois un
message de ma hiérarchie me convoquant à un entretien le 14 avril l'objet
de la convocation est sans équivoque on y dénonce mes contrôles dont
celui dans cette structure d'aide à domicile et on maintient l'ordre de
cesser mes interventions je suis sous pression mais je garde en tête que je
suis justement indépendant des pressions et des influences extérieures je
découvre dans la foulée que ma hiérarchie le préfet de département le
président du conseil départemental ma direction localement régionalement
et j'apprendrai plus tard le cabinet le cabinet de Muriel Pénicaud à leur
ministre du Travail agissent pour tenter de m'empêcher de contrôler allant
jusqu'à écrire à l'employeur de ne pas répondre à mes sollicitations
je décide de saisir par mail par courrier circonstanciel est très et
14 avril 2020 le directeur général du trail la plus haute autorité de
l'inspection du travail en France des pressions et des influences que je subis
je comprends que deux options s'offrent à moi obéir arrêter mon enquête
et mes contrôles je suis comme tout le monde confiné avec ma famille il
me suffit de regarder ailleurs de fermer les yeux de passer à autre chose
mais je n'y arrive pas je sais que ce que je fais à ce moment-là et juste
que je ne demande que l'application du droit que je suis un inspecteur du
travail indépendant des pressions étant en temps d'agir pour la santé la
sécurité et les conditions de travail des salariés le 14 avril ce sont
mes n + 1 c'est comme ça qu'on dit maintenant plus 2 + 3 + 4 qui mènent
l'entretien pour maintimer l'ordre d'arrêter mes contrôles me fixant un
ultimatum écrit par mail envoyé le 14 avril au soir jusqu'au 15 avril
à 15h le 15 avril à 16h je vais déposer devant le tribunal judiciaire
mon référé je rentre chez moi le soir et j'ai le sentiment d'avoir fait
mon travail je sais que le chemin est encore long mais que la voix et la
bonne à 20h28 je reçois un sms de ma directrice régionale de l'époque
monsieur Smith vous êtes suspendu de vos fonctions à effet immédiat la
suite les quatre points de suspension les deux ans de mutation en Meuse la
formidable mobilisation sociale syndicale politique autour de mon affaire
qui aboutira à des reculs d'Elisabeth burn qui à un moment a remplacer
madame Pénicaud vous en souvenez au ministère du Travail tout cela est
une autre histoire l'important est d'en retenir la fin le 20 octobre 2022
le tribunal administratif de Nancy que j'avais saisi va annuler purement
simplement et totalement la sanction disciplinaire et va réaffirmer le
principe d'indépendance des inspecteurs du travail en indiquant je le
cite les inspecteurs étaient tenus de respecter les instructions de leur
hiérarchie sauf si comme c'est le cas dans cette affaire les instructions
entravent et la conduite de leur contrôle quelques jours plus tard invité
à me rendre au siège de la Direction du Travail à Strasbourg j'apprendrai
que ni monsieur du soft devenu ministre du Travail ni madame born devenue
première ministre non décidé de faire appel de ses jugement du tribunal
administratif qui deviendra définitif le 20 décembre 2022 le premier
janvier 2023 je réintègrerai mon poste un inspection du travail à Reims
avec mes collègues nous n'avons rien lâché ami et camarades et à la fin
nous avons gagné [Applaudissements]
