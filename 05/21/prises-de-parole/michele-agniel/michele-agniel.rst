
.. index::
   pair: Résistante; Michèle Agniel
   ! Michèle Agniel

.. _michele_Agniel:

===========================================================================
**Michèle Agniel**
===========================================================================


Présentation
=================

Résistante dès 1940, arrêtée, déportée. (Lecture d’un texte qu’elle nous a transmis)

