
.. index::
   pair: Résistante; Pinar Selek
   ! Pinar Selek

.. _pinar_selek:

===========================================================================
**Pinar Selek** (oppression par le gouvernement turque)
===========================================================================

- https://librinfo74.fr/soutien-a-pina-selek-militante-turque-traquee-par-le-gouvernement-derdogan/

Présentation
=================

- Pinar Selek  (https://pinarselek.fr/biographie/, https://nitter.cutelab.space/Pinar_Selek/rss)

Acquittée à quatre reprises dans la même affaire, elle est la victime
d’un acharnement sans précédent dans l’histoire judiciaire de la Turquie,
dans le cadre d’un procès politique qui dure depuis un quart de siècle.

.. figure:: images/pinar_selek.png
   :align: center


Vidéo
===========

- https://piped.video/uKxG1d9kTDU
- https://www.youtube.com/watch?v=uKxG1d9kTDU


Sur les femmes iraniennes:
-----------------------------

- https://youtu.be/uKxG1d9kTDU?t=310


Transcription youtube à retravailler
-----------------------------------------


accueillons maintenant Pinard Selek depuis un quart de siècle la fatigable
intellectuelle militante féministe et défenseur des droits humains et
des minorités et réfugiés en France pour échapper à l'acharnement
judiciaire qui la cible en Turquie son pays d'origine une nouvelle audience
est prévue à Istanbul en septembre il est important de montrer au pouvoir
turc que même au bout de 25 ans nous ne lâcherons rien la liberté est une
nécessité la solidarité un impératif la justice une exigence son comité
soutien est aussi présent si vous voulez échanger à l'issue avec eux à
l'issue ils sont là de ces paroles [Applaudissements] bon je merci de me
faire partie de cette émotion et vous sentez la force moi aussi je le sens
très fort et mais je peux dire que je suis dans une période une des plus
difficiles de mon combat de mon combat contre l'État fasciste contre l'État
turc fasciste et cet état veut mener un tir depuis 25 ans j'ai résisté
malgré la torture la prison l'exil etc etc et maintenant il a insiste pour
mon extrait du et ses hommes par militaires qui ont déjà tué les Kurdes
à Paris et ailleurs et attaquer les Arméniens partout en France me menace
mon cercle et essayer de réduire mon espace quotidien parce que vous savez
bien que les grandes puissances qui sont responsables des grands malheurs
dans les planètes parce que les grandes puissances les laissent faire il y
a aucune ou gris ou gris c'est la fauchistes est en prison en France mais
beaucoup de Kurdes qui sont en France en prison en France parce que les
grandes puissances les laissent faire comme je dis parce que les loup gris
sont libres en France parce que les assassins des cures de Paris ne sont
pas arrêtés les vraies parce que la solidarité de mes chers camarades et
limitaient parce que l'élite transnationales ne sont pas prioritaires et
les taturques fascistes comme d'autres États et conscient de ça je vous
demande de participer avec moi à ce combat combat d'une sorcière contre un
État fasciste qui aujourd'hui avec John et d'autres alliances comme Qatar et
d'autres est en train de massacrer les Kurdes les Arméniens et qui recréer
une une une fausse néo conservateur né au fasciste dans les régions et je
suis une toute petite point dans ce grand tableau dans les régions je parle
au nom des centaines milieu de prisonniers politiques en Turquie maintenant
la plupart sont les Kurdes ça un millier de prisonniers politiques à peu
près 200 milliards je vous demande s'il vous plaît maintenant de ne pas
m'applaudir quand je finis quand je finis mon parole et s'il vous plaît
je voulais juste que vous conservez et on réfléchit une minute parce que
parce que moi je veux vous proposer quelque chose en France il se passe des
choses très intéressantes très fort partout dans le monde aussi c'est
bien de penser globale et agir local et global pas seulement local ou si
global vous savez je vous demande de ne pas m'applaudir parce que nous avons
applaudit les femmes iraniennes allemandes on sait pas déplacer on sait pas
mouillé on sait pas déranger les consulats des ambassadeurs on sait pas
aller tout droit on a applaudit il faut pas applaudir parce que ça fait
mourir on a applaudit les Kurdes des femmes kurdes surtout et aujourd'hui
j'ai vu le matin 12 sont morts hier soir sur les bombes de l'État turc ne
m'applaudissez pas s'il vous plaît et je vous avais peut-être que je suis
très active dans des plus de luttes en France et je vous demande de partager
ce combat que je mène aussi contre le fascisme turc merci beaucoup

Article librinfo74 **Soutien à Pina Selek, militante turque traquée par le gouvernement d’Erdogan**
=====================================================================================================

- https://librinfo74.fr/soutien-a-pina-selek-militante-turque-traquee-par-le-gouvernement-derdogan/

Introduction
--------------

Acquittée à quatre reprises dans la même affaire, Pina Selek, cette militante
turque, est la victime d’un acharnement judiciaire sans précédent dans
le cadre d’un procès politique qui dure depuis un quart de siècle.

Sociologue, écrivaine, militant antimilitariste et éco-féministe, Pinar Selek
est la cible du pouvoir turque pour avoir mené une enquête sur les kurdes.

Incarcérée, torturée, elle refuse de livrer à la police l’identité de
militants kurdes sur lesquels elle conduisait ses travaux de sociologue.


Des comités de soutien à Pinar Selek
-------------------------------------------

Partout en France, des comités de soutien mènent un travail d’information
pour sensibiliser les citoyens à la cause de Pinar.

Déjà condamnée à perpétuité, un nouveau procès se tiendra à Istanbul le
29 septembre 2023.

Pinar Selek risque une nouvelle fois la prison à perpétuité.

Une délégation est prévue depuis la France pour assister au procès, et
montrer aux juges que la solidarité n’a pas de frontières.

Joana, présente sur le plateau des Glières, aux côtés de Pinar, appelle
à créer partout en France de nouveaux comités de soutien :




