
.. _crha_2023_05_21:

===================================================================================
|crha| Dimanche 21 mai 2023 10h30 **Prises de parole au plateau des Glières**
===================================================================================

- https://www.youtube.com/@CRHA_RassemblementdesGlieres/videos
- https://citoyens-resistants.fr/spip.php?article659
- https://citoyens-resistants.fr/IMG/pdf/tract_recto_verso_impression_crha_2023.pdf
- https://citoyens-resistants.fr/IMG/pdf/crha_programme_mai_2023.pdf
- https://fr.wikipedia.org/wiki/Plateau_des_Gli%C3%A8res
- https://www.geoportail.gouv.fr/carte?c=6.329477350590899,45.96409759654725&z=17&l0=GEOGRAPHICALGRIDSYSTEMS.MAPS::GEOPORTAIL:OGC:WMTS(1)&l1=GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2::GEOPORTAIL:OGC:WMTS(1)&l2=ORTHOIMAGERY.ORTHOPHOTOS::GEOPORTAIL:OGC:WMTS(1)&permalink=yes
- https://app.webcam-hd.com/conseil-general-74/cg74_plateau-des-glieres


.. figure:: ../images/monument_des_glieres.png
   :align: center
   :width: 500

   https://fr.wikipedia.org/wiki/Monument_national_%C3%A0_la_R%C3%A9sistance_du_plateau_des_Gli%C3%A8res


.. raw:: html

   <iframe width="800" height="600" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" sandbox="allow-forms allow-scripts allow-same-origin"
       src="https://www.geoportail.gouv.fr/embed/visu.html?c=6.329477350590899,45.96409759654725&z=17&l0=GEOGRAPHICALGRIDSYSTEMS.MAPS::GEOPORTAIL:OGC:WMTS(1)&l1=GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2::GEOPORTAIL:OGC:WMTS(1)&l2=ORTHOIMAGERY.ORTHOPHOTOS::GEOPORTAIL:OGC:WMTS(1)&permalink=yes" allowfullscreen>
   </iframe>


   https://app.webcam-hd.com/conseil-general-74/cg74_plateau-des-glieres


.. figure:: images/montage.png
   :align: center

.. figure:: images/montage_2.png
   :align: center

.. figure:: ../images/glieres_nouvelles_nouveaux_resistantes.png
   :align: center


.. figure:: ../images/resistantes_fin_de_prises_de_parole.png
   :align: center

   https://nitter.net/Gilles_Perret/status/1660235771285065729#m


.. figure:: images/chant_des_partisans.png
   :align: center


.. figure:: images/sous_chapiteau.png
   :align: center


Vidéos
=======

- https://www.youtube.com/@CRHA_RassemblementdesGlieres/videos
- :ref:`videos_2023_05_21`


Angélique Huguin
=================

.. toctree::
   :maxdepth: 3

   angelique-huguin/angelique-huguin


Anthony Smith
====================

.. toctree::
   :maxdepth: 3

   antony-smith/anthony-smith

George Ibrahim Abdallah
============================

Plus vieux prisonnier politique d’Europe.
Texte de prison lu par Nicolas Berry, membre du comité de soutien et réalisateur.

Julien Le Guet
===================

.. toctree::
   :maxdepth: 3

   julien-le-guet/julien-le-guet


Lincoyan Huilcamán
=======================

.. toctree::
   :maxdepth: 3

   lincoyan-huilcaman/lincoyan-huilcaman


Michèle Agniel
=======================

.. toctree::
   :maxdepth: 3

   michele-agniel/michele-agniel

Paul François
==================

.. toctree::
   :maxdepth: 3

   paul-francois/paul-francois


Pinar Selek
====================

.. toctree::
   :maxdepth: 3

   pinar-selek/pinar-selek


Sophie Djigo
==================

.. toctree::
   :maxdepth: 3

   sophie-djigo/sophie-djigo



Paul François
==================

.. toctree::
   :maxdepth: 3

   paul-francois/paul-francois

