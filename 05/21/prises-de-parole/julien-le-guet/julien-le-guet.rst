
.. index::
   pair: Résistant; Julien Le Guet
   ! Le peuple de l'eau

.. _julien_le_guet:

====================================================================
**Julien Le Guet, porte parole des Soulèvements de la terre**
====================================================================


- https://piped.video/watch?v=zQ3z5Ee6hm0


Présentation
=================

Porte-parole du mouvement Bassines non merci, gardé à vue, puis sous
contrôle judiciaire pour ses actions.

Les « Paroles de Résistances «  se terminent avec Julien Le Guet de « Bassines non merci » sur fond du chant des partisans
==============================================================================================================================

Les « Paroles de Résistances «  se terminent avec Julien Le Guet de
« Bassines non merci » sur fond du chant des partisans.

⁦@Fakir_⁩ ⁦@Politis_fr⁩ ⁦@blast_france⁩ ⁦@LeMediaTV⁩


.. figure:: images/coeur.png
   :align: center

   https://nitter.net/Gilles_Perret/status/1660235771285065729#m



Vidéo de Julien Le Guet porte parole des Soulévements de la terre (18 minutes)
=======================================================================================

- https://www.youtube.com/watch?v=zQ3z5Ee6hm0
- https://piped.video/watch?v=zQ3z5Ee6hm0


Transcription Youtube à retravailler
----------------------------------------

[Applaudissements] peut-être essayer d'apporter un petit peu de légèreté
prendre un petit peu de hauteur j'ai été un mauvais élève j'ai rien
écrit plein de choses et puis on va y aller comme ça et puis déjà faire
un hommage aux copains antifa des Deux-Sèvres qui sont très très contents
qu'un chamois niait tu dédicace aux copains de mails qui se battent contre les
bassines et qui nous ont accueillis quand lors de la bataille de sainte Soline
et ce petit canard il se balade partout pour dire que vous nous accueillez
on vous accueille on vous en remercie venir nous aider à chaque fois comme
ça quand on reçoit toute cette énergie comme on l'a reçu à Sainte Soline
le 25 mars là on est redevable de quelque chose et on comprend pleinement
ce que représente la force l'Union la solidarité la convergence des luttes
voilà ensemble on vaincra alors est-ce que ça signifie pour moi de monter
ici de répondre à l'invitation des camarades de Gilles de Marion de tous
ces bénévoles qui s'activent et qui rendent ces conditions possibles j'ai
eu l'occasion d'entendre parler des Glières à l'école un petit peu trop
peu j'ai l'occasion d'entendre parler des Glières quand j'ai appris que le
Sarko il voulait venir ici avec un hélico et puis venir à la fois faire
la double arnaque de broyer un héritage qui est celui du programme du CNR
tout en voulant hériter de la moralité du CNR et donc on est là il est
plus là et sa place est en prison [Applaudissements] ça fait penser à
Stéphane Hessel bien sûr et puis ça fait penser que il y a 80 ans il y a
6000 monstres et dans ces 6000 monstres il y avait 2000 français d'accord
qui est qu'on a appelé milice mais qui est pas devenu par la suite devenu
la gendarmerie mobile les mêmes les mêmes barbares qu'il 25 mars pour
défendre un cratère ou lancer 5000 bombes sur le peuple de l'eau alors
le plateau des Glières c'est devenu déjà il y a deux ans pour nous les
camarades de Notre-Dame-des-Landes bah si non merci un lieu de pèlerinage
ou à chaque fois qu'on passe ici on désobéit on vient mettre une affiche
sur un lieu de mémoire voilà et à chaque fois qu'on passe voilà et donc
quand Gilly il a passé un coup de fil il a dit bah ouais ben qu'on ne se
peut que venir et on reviendra je vous invite à venir nous voir je dois
saisir les 10 minutes de pas être trop long alors après puisqu'il fallait
un peu parler de soi et tout ça mais bon c'est pas trop intéressant mais
quand même il y avait une forme d'obligation de venir parce que Ben dans le
collectif on est quelques-uns avoir la fonction de porte-parole et puis ben
voilà là je suis ambition de porte-paro là et c'est une mission qui est un
peu comme tous les camarades qu'on parlait c'est une mission qui expose c'est
une mission qui fait qu'on m'a surtout quand on est des collectifs et puis
qu'on on fait en sorte de pas se un peu quand même soulèvement de la terre
donner le bâton pour se faire battre en créant des assos etc ou d'ailleurs
il y a toutes les mesures de rétorsion ben évidemment que Darmanin et sa
bande ils n'ont qu'une solution c'est de taper sur les têtes qui dépassent
et donc voilà ça nous expose notamment angélique elle a 5 ans d'avance
mais je crois qu'on est toute une bardée dans le Marais à suivre le même
parcours juridique dans les mois enfin c'est des gens enclenché mais ça
nous pend au nez quoi alors ben quand on porte la parole puis qu'on monte
ben je l'ai dit il faut remercier les gens qui créent les conditions donc
pour faire ça il y a une mission qui est témoignée je vais vous donner
des nouvelles de sainte-soly et puis de la suite évidemment et puis il
y a un truc qui est incroyable et que j'avais largement sous-estimé mais
voilà c'est fin quand on voit le casting aussi on se dit mais faut venir
tellement de gens merveilleux à rencontrer tout ce qu'on parlait et puis
tout ce que vous êtes qui parlent on se rend compte mais les poils quoi
angélique waouh c'est Julie Julie elle est hydrogéologue elle se bat pour
le lampes Palestine alors évidemment ça va être une alliée pour la suite
Julie tu es là on va se revoir et se revoir et avec toi on va on va vaincre
c'est sûr et puis avec toi on va monter les accords d'Oslo et après ben
je vais m'arrêter là sur ce qui est des un petit peu de du truc personnel
parce que en fait quand on fait être conscient que quand on en est à ces
trucs de porte-paro là c'est que des trucs qu'on a loupé c'est qu'il y a
tout le monde médiatique etc qui font fonctionner comme ça mais en fait
c'est pas juste parce qu'on est des collectifs et donc c'est pour ça que
les soulèvements de la terre ils ont pas de porte parole identifier comme
ça c'est pour ça qu'aujourd'hui un mouvement qui s'appelle naturaliste des
terres mais maintenant les porte-parole c'est des animaux c'est des camarades
qui portent des masques d'animaux et ça me fait penser aussi commandant
Marcos mais il porte la cagoule voilà et donc voilà c'est ça explose etc
en fait c'est pas parfait faut réussir à dépasser ça et à réussir à
pas personnifier les combats et voilà donc je vous remercie vraiment de
tous les témoignages que vous avez pu m'accorder depuis hier etc ça fait
chaud au cœur et c'était maillage d'amitié et de soutien etc j'ai qu'une
envie c'est de s'en occuper et de les repartager voilà cool merci à tous
c'est pas fini largement pas mais je vais m'accélérer [Applaudissements]
j'avais promis qu'on allait désobéir alors c'est un truc c'est une première
désobéissance mais j'ai vu que le camarade mappucci l'avait fait il y a
un préfet qui dit que ici par respect pour la mémoire on met pas d'affiche
quoi et on peut atterrir avec un hélico non franchement n'importe quoi puis
quand il y a les facho qui viendront avec leur crois celtique et cetera elle
préfère le fera rien non mais oh et donc désobéissance donc je voulais
apporter tout mon soutien à travers vous aux camarades et soulèvement de
la terre [Applaudissements] camarade et soulèvement la terre donc vous le
savez l'ignominum Darmanin essaye d'obtenir vainement la dissolution et comme
borne semble lui dire maintenant mais j'ai rare on ne peut pas dissoudre un
mouvement on ne peut pas dissoudre une idée non c'est pas général c'est
pas Isabelle qui a dit ça c'est Monsieur Descola et donc ouais on parle
pas dans un carnet de mettre un peu légèreté mais traiter des côtes
terroristes et que le soir de sainte Soline sa première réaction à 18h
dans les médias c'est de revendiquer l'attentat l'utilisation de 4000
la vérité c'est 5000 bonbons arme de guerre gm2l tire tendu LBD canon
la totale l'opération militaire comme on n'avait pas vu en France depuis
longtemps utilisé contre le peuple de l'eau la honte et je crois que dans
ce contexte là en fait je me demande si shocking c'est le mode résistance
il est pas insuffisant j'entends dans résistance comme justifier le fait
qu'on résiste on fait face en fait c'est pas suffisant il faut passer
à l'offensif puis pas de résister alors il veut nous dissoudre ben va
prendre racine une réponse qu'on vous encourage à faire c'est que suite
à la menace des dissolutions il y a eu un appel à créer des collectifs
locaux 180 collectifs locaux aujourd'hui sont en place il y en a forcément
un près de chez vous et c'est des lieux que je vous invite à rejoindre
parce que c'est des lieux de large convergence c'est des lieux où on vient
avec nos drapeaux ou pas qu'on fasse partie d'une tribu ou pas et on essaye
de repenser la manière d'agir ensemble on essaye de dépasser de faire en
sorte que nos cultures et de manière d'agir soient des atouts plutôt que
des éléments différenciation et à travers les luttes comme on les porte
avec les camarades soulèvement de la terre avec la Confédération paysanne
avec les antifas avec les syndicats avec Sud avec la CGT avec les parties etc
etc on apprend à faire ensemble et on apprend de toute façon nous n'avons
pas le choix c'est lutte nous devons les gagner et nous n'avons pas le choix
face à nous les comment dire nos oppresseurs ont des stratégies qui sont
internationales les oppresseurs ne s'embarrassent pas de leur petit distinguo
et de savoir où est-ce qu'il vaut mieux placer son argent ils font encore ils
sont barbares et nous face à ça nous devons être unis et vous dit vraiment
arrêtez de se dire c'est pas comme ça qu'il faut agir il y en a concours
c'est à dire comme ça je veux dire de manière assez claire les Black Blocs
ne sont pas des casseurs le Black Blocs sont les garants de nos libertés de
manifester aujourd'hui c'est eux qui ont le courage d'aller devant au devant
de la milice d'armanière et de faire en sorte que nous citoyens peut-être
pas aussi capable d'aller au devant bah nous puissions simplement marcher
là où nous habitons marcher pour défendre l'eau marcher pour dénoncer
au niveau international le scandale incroyable que peut représenter une de
méga bassines j'ai dit un gros mot et j'en arrive vous avez promis que je
vous donnerai des nouvelles alors j'ai des bonnes nouvelles les camarades
les 200 camarades qui ont été mutilés dans les trois camarades sont sortis
du coma et entame aujourd'hui le long chemin de la reconstruction il auront
besoin de nous mais ils aurons une vie après sainte Soline elle sera belle
elle sera déterminée ils sont debout et avec eux tous les camarades compris
ces multiples éclats qu'on a vu boiter il remarche le droit il remarche de
bouche ça c'est pour les bonnes nouvelles à côté de ça le concert bassine
continue à étaler ses métastases à travers le territoire à travers la
France mais aussi à travers tous les autres pays qui n'attendent que de
voir ce que donnera le résultat de la bataille de sainte-soly ils sont tous
sur les sardines blocs Limagrain pas très loin d'ici enfin sur le chemin
entre ici et prêt à faire deux mégabits de 1 million de mètres cubes un
truc énorme pour faire la séance de maïs la maïs je vais pas revenir
là-dessus ils sont prêts ils sont prêts et d'où l'importance de mener
tous ensemble ces victimes bataille qui s'annonce sur Sainte Soline prochaine
bataille c'est la bataille du plastique parce que faut pas l'oublier ce qui
rend intolérable aussi ses bassine c'est que c'est des océans de plastique
il y a là 16160 mille mètres carrés d'un plastique épais de chambre à
air qu'ils veulent étaler pour artificialiser nos sols et capter l'eau donc
ça on va pas laisser faire et donc on va vous inviter à nous rejoindre pour
ces batailles là et puis également je vais vous donner des nouvelles des
camarades qui sont primés des camarades qui ont fait l'objet d'agression on
ne veut Valentin qui s'est fait agresser à son domicile il y a quatre mois
de ça certainement en coupera avec Paul dans les mêmes salopards qui sont
venus l'agresser à son domicile parce qu'en fait ils sont à moins de 30
km et j'ai juste au-dessus et aujourd'hui fallait que je vais au Plateau des
Glières pour me rendre compte de ça donc merci d'invitation merci Paul on
va discuter évidemment [Applaudissements] alors alors la lutte elle continue
évidemment que vous avez dit on peut pas perdre ces batailles là c'est
des batailles qui sont vitales les batailles pour la conservation du CNR et
de tout ce qui a fait notre tissu social et notre solidarité elles doivent
être maintenues au même titre aujourd'hui la bataille pour la sauvegarde
de notre espèce et des espèces doit être une priorité absolue ce matin
le Béchu annonçait que le scénario le plus crédible c'était plus 4
degrés ok en 2100 donc ça y est ils le disent ils ont fait beaucoup de
chemin en 4 mois puisque je vous rappelle que Macron disait encore pour
le réveillon mais qui aurait pu imaginer je pense que j'allais parler de
Macron aucun intérêt et donc vous vous en doutez vous le savez on le sait
tous il y a un été qui s'annonce effroyable le plus chaud qui soit le plus
significatif en termes de sécheresse et donc on sait que d'une manière ou
d'une autre des actions qui vont être menées partout en France et donc
les peuples de l'eau dans lesquels nous sommes et je vais pas mentir que
je participe activement à échafauder ce genre de sympathique invitation
nous allons lancer un appel sans jour pour les sécher 100 jours pour les
submerger sans le jour pour les inonder ça commence à partir vous aurez
début juin jusqu'au 28 septembre jusqu'à après les procès et là ça va
être son jour de créativité on espère voir fleurir partout en France
pour venir contrer l'ignoble le lien signifiant plan haut de Macron qui
veut nous faire croire que c'est en mettant des bains moussants des non oh
là là des bouchons mains bouchons moussants dans les collectivités qu'on
va résoudre le problème et alors que en face de ça fait l'eau annoncer
au congrès de la FNSEA qu'on demanderait on demanderait aucun effort aux
principales secteur consommateur qui est le maïs en France qui consomme 25%
de l'intégralité de l'eau douce et de l'agriculture en général irriguante
et principalement pour la grande industrie qui consomme 50% de l'eau douce et
donc on leur demanderait pas d'effort on demanderait pas non plus d'effort
aux bourgeois pour remplir la piscine on demande un pas non plus d'effort
on pourrait continuer à arroser des stades de foot à arroser des stades
de golf donc voilà vous avez bien compris l'esprit donc c'est en fait on
donnera quelques pistes il vous manquez de créativité mais on sait que
vous n'en manquerez pas moi je sais pas des poissons morts dans les piscines
pourquoi pas à l'époque on ne les années 70 il y avait je sais pas pourquoi
je vous parle de ça mais il y a un groupe d'activistes dans le truc qui
était de laisser des étrons dans les piscines à bourgeois bon voilà un
peu de légèreté prenons la hauteur c'est peut-être l'ivresse des signes
ce que c'est moi je dis n'importe quoi nous allons également annoncer je
vous annonce là ça je suis habilité à le faire une grande marche en
vélo en tracteur une un grand convaincre qui partira de sainte Soline le
19 août pour aller jusqu'à la maison mère de l'agence de Loire Bretagne
et le 24 août c'est une grande baignade certainement à côté de Paris on
va vous dire tout ça et il semblerait qu'il y ait des convois des montagnes
qui descendent alors c'est facile ça descend [Musique] et pour finir il y
a une autre proposition désobéissance et qui est pas une désobéissance
par absence de respect je suis désobéissance à scander ensemble si vous
le voulez bien de ce logo est-ce que vous êtes partant c'est le slogan des
soulèvements debout la terre sous lève-toi Fribourg la terre sous lève-toi
debout la terre [Applaudissements] debout la terre sous lève-toi debout la
terre soulève-toi debout la terre soulève toi merci ça semble m'avait dit
10 minutes émotions frissons compris bon je déborde ce sera 10 minutes 15
minutes le grand compris désolé j'ai fini un dernier une dernière demande
s'il vous plaît pour les peuples de l'eau que nous sommes pour les peuples
debout dans la boue qui ont de sainte Soline et pour les peuples de bouc
nous étions nous resterons parce que c'est la lutte elle nous aide à ça
à nous tenir debout et être pleinement des hommes et des femmes pour les
camarades qui sont restés au fin fond des marais si seulement on pouvait
faire le plus gros nos basses arabes et faire écho et espérer leur envoyer
toutes ces ondes aux camarades qui se retapent aux camarades qui ont besoin
de tout soutien merci à vous non bah ça va [Applaudissements]

