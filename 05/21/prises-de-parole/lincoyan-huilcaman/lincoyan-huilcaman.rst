
.. index::
   pair: Résistant; Lincoyan Huilcaman
   ! Lincoyan Huilcaman

.. _lincoyan_huilcaman:

===========================================================================
**Lincoyan Huilcaman - la parole d'un résistant**
===========================================================================


Présentation
===============

Résistant et orateur chilien du peuple Mapuche.
Filmé au Rassemblement des Glières 2023

Vidéo
========

- https://piped.video/watch?v=-06yUsgLeH4
