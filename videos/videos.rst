.. index::
   ! Vidéos

.. _videos:

=========================================
**Vidéos**
=========================================

- https://www.youtube.com/@CRHA_RassemblementdesGlieres/videos


@UnionSolidaires est présente sur le @CRHA_glieres pour porter un "discours qui favorise l'émancipation des populations"
===========================================================================================================================

- https://nitter.net/CRHA_glieres/#m

@UnionSolidaires est présente sur le @CRHA_glieres pour porter un "discours
qui favorise l'émancipation des populations".

Isabelle et Marine au micro de @Cemil. ⤵️

.. figure:: images/isabelle_marine.png
   :align: center



.. _videos_2023_05_20:

Vidéos du samedi 20 mai 2023 (Ce lien expire le 21/06/2023 à 17:06)
=========================================================================

- https://www.swisstransfer.com/d/c922bc2a-7c80-4ec6-bca3-b46a5655c689


.. _videos_2023_05_21:

Vidéos du dimanche 21 mai 2023 (Ce lien expire le 21/06/2023 à 17:26)
=========================================================================

- https://www.swisstransfer.com/d/964470ff-8b4c-40fb-9195-680e03f41ba2
